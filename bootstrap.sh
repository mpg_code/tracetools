#!/bin/sh

## set compiler
if [ "`command -v clang++`" = "" ];
then
    sed -i 's/^CPP.*=.*/CPP=g\+\+/' Makefile
fi
