/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#include <sstream>
#include <vector>
#include <queue>
#include "Analyzer.h"
#include "ProgressBar.h"
#include "LinReg.h"


//-------------------------------------------------------------------------------------
Analyzer::Analyzer()
{
}
//-------------------------------------------------------------------------------------
Analyzer::~Analyzer()
{
}
//-------------------------------------------------------------------------------------
void* Analyzer::iatStat(StreamStats* ss, StreamDetails *det, bool is_tcp, GSMap* gsmap, pthread_mutex_t *gsmap_mutex, FileManager *fm)
{
    det->pcnt = ss->plList.size();
    int iat_samples = ss->iatVector.size();
    if (is_tcp) {
        det->acnt = ((StreamStatsTCP*)ss)->ackcount;
       /* if (det->acnt > det->pcnt) {
            det->stream_category = 6;
            return 0;
        }*/
    }
    if (iat_samples == 0)
        return 0;

    if (iat_samples < 3) {
        stringstream out;
        det->iat_median = ss->iatVector.back()->m_value;
        if (iat_samples == 1) 
            out << "1(" << (int)(ss->iatVector.front()->m_value/1000) << ":1)";
        else 
            out << "1(" << (int)((ss->iatVector.front()->m_value + ss->iatVector.back()->m_value)/2/1000) << ":2)";
        det->iat_kmeans_clusters = out.str();
        if (det->complete == 1 || (det->complete == 2 && det->rst))
            det->stream_category = 7;
        return 0;
    }
    // exclude streams that do not seem to have mss-sized packets at this stage
    bool mss_test_passed = 0;
    uint16_t max_pl = 0;
    for (list<uint16_t>::iterator itPl = ss->plList.begin(); itPl != ss->plList.end(); ++itPl) {
        if (*itPl > max_pl)
            max_pl = *itPl;
    }
    // assume mss to be max payload size that is above min mss
    if (max_pl >= 536) 
        mss_test_passed = 1;
    
    uint64_t mss_pkts = 0; 
    if (mss_test_passed) {
        uint64_t psh_pkts = 0;
        int i = ss->iatVector.size();
        vector<ListElem*>::iterator itIat = ss->iatVector.begin();
        for (list<uint16_t>::iterator itPl = ss->plList.begin(); itPl != ss->plList.end(); ++itPl) {
            if (*itPl == max_pl) {
                mss_pkts++;
                if ((*itIat)->m_psh)
                    psh_pkts++;
            }
            if (--i != 0)
                ++itIat;
        }

        // exclude streams with majority of packets below mss at this stage
        uint64_t pkt_count = ss->plList.size();
      
        if (mss_pkts == psh_pkts || mss_pkts*100/pkt_count < NVALUE) 
            mss_test_passed = 0;
    }
    uint64_t nr_values = ss->iatVector.size();
    vector<ListElem*> iatVector_copy = ss->iatVector;
    sort(iatVector_copy.begin(), iatVector_copy.end(), ListElem::compare); 
    vector<ListElem*>::iterator it = iatVector_copy.begin();
    advance(it, percentile(50, nr_values)-1);
    det->iat_median = (*it)->m_value;
    
    if (!mss_test_passed || !is_tcp)
        return 0;

    list<KMeansCluster>* clusters;
    clusters = new list<KMeansCluster>;
    uint32_t min_value = iatVector_copy.front()->m_value/1000;
    uint32_t max_value = iatVector_copy.back()->m_value/1000;

    uint32_t range = max_value - min_value;
    if (range > 0) {
        // step 1: k = 2
        KMeansCluster newcluster_min((float)min_value);
        clusters->push_back(newcluster_min);
        KMeansCluster newcluster_max((float)max_value);
        clusters->push_back(newcluster_max);

        // step 2:  Assign each sample to the cluster with nearest mean.
        vector<ListElem*>::iterator itIat = iatVector_copy.begin();
        while (itIat != iatVector_copy.end()) {
            float min_distance = range;
            float distance;
            list<KMeansCluster>::iterator matching_cluster;
            for (list<KMeansCluster>::iterator cur_cluster = clusters->begin(); cur_cluster != clusters->end(); cur_cluster++) {
                distance = fabs(cur_cluster->m_mean - (float)((*itIat)->m_value/1000));
                if (distance < min_distance) {
                    min_distance = distance;
                    matching_cluster = cur_cluster;
                }
            }
            matching_cluster->m_values.push_back(*itIat);
            ++itIat;
        }
     
        // step 3: Recalculate means for each cluster and reassociate the values until convergence. 
        bool convergence = false;
        while(!convergence) {
            list<KMeansCluster>::iterator cur_cluster;
            list<KMeansCluster>::iterator next_cluster;
            convergence = true;
            for (list<KMeansCluster>::iterator cur_cluster = clusters->begin(); cur_cluster != clusters->end(); cur_cluster++) {
                next_cluster = cur_cluster;
                next_cluster++;
                if (next_cluster == clusters->end())
                    break;
                cur_cluster->updateMean();
                next_cluster->updateMean();
                cur_cluster->sort();
                next_cluster->sort();
                vector<ListElem*>::iterator cur_le = cur_cluster->m_values.begin();
        
                while (cur_le != cur_cluster->m_values.end()) {
                    if (fabs(float((*cur_le)->m_value/1000) - cur_cluster->m_mean) > fabs(float((*cur_le)->m_value/1000) - next_cluster->m_mean)) {
                        next_cluster->m_values.push_back(*cur_le);
                        cur_le = cur_cluster->m_values.erase(cur_le);
                        convergence = false;
                    } else 
                        cur_le++;
                }
                vector<ListElem*>::iterator next_le = next_cluster->m_values.begin();
                while (next_le != next_cluster->m_values.end()) {
                    if (fabs(float((*next_le)->m_value/1000) - next_cluster->m_mean) > fabs(float((*next_le)->m_value/1000) - cur_cluster->m_mean)) {
                        cur_cluster->m_values.push_back(*next_le);
                        next_le = next_cluster->m_values.erase(next_le);
                        convergence = false;
                    } else 
                        next_le++;
                }
            }
        }
    } else {
        KMeansCluster newcluster_min((float)min_value);
        clusters->push_back(newcluster_min);
        clusters->front().m_values = ss->iatVector;
    }
  
    // test output
    int nr_clusters = clusters->size();
    stringstream clusters_info;
    clusters_info << nr_clusters << "(";
    for (list<KMeansCluster>::iterator it_cl = clusters->begin(); it_cl != clusters->end(); it_cl++) {
        if (it_cl != clusters->begin())
            clusters_info << ",";
        clusters_info << (int)it_cl->m_mean << ":" << it_cl->m_values.size();
    }
    clusters_info << ")";
    det->iat_kmeans_clusters = clusters_info.str();
    uint64_t keepalive_breaks = clusters->front().getKeepAliveBreaks();
    uint64_t front_cluster_size = clusters->front().m_values.size();
    if (keepalive_breaks < front_cluster_size/2 && front_cluster_size > nr_values/2) 
        streamCategoryStat(ss, clusters, det, max_pl, nr_values, mss_pkts, gsmap, gsmap_mutex, fm);
    delete clusters;
    return 0;

}
GStreamStats* mapGroupStream(StreamStats *ss, StreamDetails *det, GSMap *gsmap, pthread_mutex_t *gsmap_mutex) 
{
    SrcDst_IP sd_ip = SrcDst_IP(det->src_ip.ipv4, det->dst_ip.ipv4);
    GStreamStats *gss;
    pthread_mutex_lock(gsmap_mutex);
    if (gsmap->find(sd_ip) == gsmap->end()) {
        gss = new GStreamStats();
        gss->srcport = det->src_port;
        if (det->src_port == 0)
            gss->http = 0;
    } else
        gss = gsmap->at(sd_ip);

    if (det->src_port != gss->srcport) { 
        gss->same_srcport = 0;
        gss->srcport = det->src_port;
    }
    gss->nr_streams++;
    gss->flowsize_bytes += det->flowsize_bytes;
  
    return gss;
}
//-------------------------------------------------------------------------------------
void* Analyzer::streamCategoryStat(StreamStats* ss, list<KMeansCluster> *clusters, StreamDetails *det, uint16_t segment_size, uint64_t nr_values, uint64_t mss_pkts, GSMap* gsmap, pthread_mutex_t *gsmap_mutex, FileManager *fm) 
{
    if (mss_pkts == nr_values) {
        det->stream_category = 1;
        return 0;
    }
    //GStreamStats* gss = mapGroupStream(ss, det, gsmap, gsmap_mutex);
    int i = 0;
    uint32_t train_length = 0;
    uint32_t trains_iat = 0;
    uint32_t trstart_interv = 0;
    uint32_t small_segs = 0;
    uint32_t trains_nr = 0;
    uint8_t stream_category = 0;
 
    list<uint32_t> tr_len_list;
    list<uint32_t> tr_iat_list;
    list<uint32_t> trstart_interv_list;

    vector<ListElem*>::iterator itIat = ss->iatVector.begin();
    i = 0;
    uint64_t arrival_time = 0;
    uint32_t iat = 0;
    bool train_start = false;
    uint32_t trainnr = 0;
    uint64_t ts_start_us = (uint64_t)((ss->init_time.tv_sec%12000)*1000 + ss->init_time.tv_usec);
    LinearRegression lr;
    uint32_t pl_samples = ss->plList.size();
    for (list<uint16_t>::iterator itPl = ss->plList.begin(); itPl != ss->plList.end(); ++itPl) { 
        if (*itPl == segment_size) { 
            if (!train_start) {
                train_start = 1;
                const double x = (double)(ts_start_us + arrival_time/1000);
                const double y = (double)trainnr++;
                lr.addXY(x, y);
                //gss->lr.addXY(x, y);
            }
            if (trains_iat > 0 && trains_nr > 0) {
                tr_iat_list.push_back(trains_iat);
                trains_iat = 0;
                trstart_interv_list.push_back(trstart_interv);
                trstart_interv = 0;
            }
            train_length+=*itPl;
        } else {
            if (stream_category == 0) {
                if (small_segs == 0 && (train_length > 0 || (det->complete == 3 || det->complete == 0)))
                    stream_category = 2;
                else
                    stream_category = 3;
            }
            if (i < pl_samples-1) 
                trains_iat += (*itIat)->m_value;
            if (train_length > 0) {
                train_length+= *itPl;
                tr_len_list.push_back(train_length);
                trains_nr++;
                train_length = 0;
                small_segs = 0;
                train_start = false;
            } else {
                small_segs++;
                if (stream_category != 3)
                    stream_category = 3;
            }
        } 
        i++;
        if (itIat != ss->iatVector.end()) {
            trstart_interv += (*itIat)->m_value;
            ++itIat;
        }
        if (i < pl_samples-1) {
            iat = (*itIat)->m_value;
            arrival_time += (uint64_t)iat;
        }
    }
    if (train_length > segment_size) {
        tr_len_list.push_back(train_length);
        trains_nr++;
        if (stream_category == 0) 
            stream_category = 1;
    }
    if (lr.haveData()) {
        det->packet_train_lr_det = lr.getCoefDeterm();
        det->packet_train_lr_cor = lr.getCoefCorrel();
    }
    det->packet_trains_nr = trains_nr;
    det->stream_category = stream_category;

    stringstream train_length_out;
    if (trains_nr > 0) {
        uint64_t tot_tr_len = 0;
        i = 0;
        tr_len_list.sort();
        for (list<uint32_t>::iterator it = tr_len_list.begin(); it != tr_len_list.end(); ++it) {
            tot_tr_len += *it;
	    train_length_out << *it << endl;
            if (++i == percentile(50,trains_nr))
                det->packet_train_length_median = *it;

        }
        det->packet_train_length_avg = tot_tr_len/trains_nr;
    }
    fm->write(train_length_out.str(),false);
    uint64_t tot_tr_iat = 0;
    uint64_t tot_tr_int = 0;
    i = 0;
    if (trains_nr > 1) {
        tr_iat_list.sort();
        trstart_interv_list.sort();
        list<uint32_t>::iterator itTrInt = trstart_interv_list.begin();

        for (list<uint32_t>::iterator it = tr_iat_list.begin(); it != tr_iat_list.end(); ++it) {
            tot_tr_iat += *it;
            tot_tr_int += *itTrInt;
            if (++i == percentile(50,trains_nr-1)) {
                det->packet_train_iat_median = *it;
                det->packet_train_int_median = *itTrInt;
            }
            ++itTrInt;
        }
        det->packet_train_iat_avg = tot_tr_iat/(trains_nr - 1);
        det->packet_train_int_avg = tot_tr_int/(trains_nr - 1);
    }
    return 0;
}
//-------------------------------------------------------------------------------------
void* Analyzer::plStat(StreamStats* ss, StreamDetails *det, bool is_tcp)
{
    if (ss->plList.empty())
        return 0;
    uint64_t pl_total = 0;
    
    int i = 0;
    list<uint16_t> plList_copy = ss->plList;;
    plList_copy.sort();

    if (is_tcp) 
        det->flowsize_bytes = ((StreamStatsTCP*)ss)->prev_packet.seq + ((StreamStatsTCP*)ss)->prev_packet.pl_sz - 1;
    else {
        for (list<uint16_t>::iterator it = plList_copy.begin(); it != plList_copy.end(); it++) 
            pl_total += (uint64_t)*it;
        det->flowsize_bytes = pl_total;
    }
    list<uint16_t>::iterator it = plList_copy.begin();
    advance(it, percentile(50, det->pcnt)-1);    
    det->payload_median = *it;  
    return 0;  

}
//-------------------------------------------------------------------------------------
void* Analyzer::rttStat(StreamStats* ss, StreamDetails *det)
{
    if (ss->iatVector.empty())
        return 0;

    vector<ListElem*>::iterator it = ss->iatVector.begin();
    int j = 0;
    uint32_t rtt_total = 0;
    int rtt_samples = 0;
    while (it != ss->iatVector.end() && j < 10) {
        if ((*it)->m_sa_ack) 
            it = ss->iatVector.erase(it);
        else if ((*it)->m_rtt_sa) {
            rtt_total += (*it)->m_value;
            rtt_samples++;
            it = ss->iatVector.erase(it);
        } else if ((*it)->m_rtt_ss) {
            rtt_total += (*it)->m_value;
            rtt_samples++;
            it++;
        } else 
            it++;
        j++;
    }
    if (rtt_total > 0) 
        det->rtt_average = rtt_total / rtt_samples / 1000;
  
    return 0;
}
//-------------------------------------------------------------------------------------
void* Analyzer::stat(StreamStats *ss, StreamDetails *det, bool is_tcp, GSMap* gsmap, pthread_mutex_t *gsmap_mutex, FileManager *fm)
{
    struct timeval end_time;
    timediff_add(ss->init_time, ss->prev_time, end_time);
    
    struct timeval tot_duration;
    timersub(&end_time, &(ss->init_time), &tot_duration);

    det->duration = (float)tot_duration.tv_sec;
    det->duration += (float)tot_duration.tv_usec/1000000;
    if (det->duration > 11000) {
        cout << "src_ip: " << det->src_ip.ipv4 << " dst_ip:" << det->dst_ip.ipv4 << endl;
        cout << "duration: " << det->duration << " ss->prev_time: " << ss->prev_time.tv_sec << ":" << ss->prev_time.tv_usec << " " << " calculated tot_duration: " << tot_duration.tv_sec << ":" << tot_duration.tv_usec << endl;
    }
    det->start_time = ss->init_time;

    if (is_tcp) {
        rttStat(ss, det);
        StreamStatsTCP* sstcp = (StreamStatsTCP*) ss;
        if ((sstcp->syn || sstcp->synack) && sstcp->fin)
            det->complete = 1;
        else if (sstcp->syn || sstcp->synack) 
            det->complete = 2;
        else if (sstcp->fin)
            det->complete = 3;
        if (sstcp->rst)
            det->rst = 1;
        if (sstcp->zero_win)
            det->zero_win = 1;
    }
    plStat(ss, det, is_tcp);
    iatStat(ss, det, is_tcp, gsmap, gsmap_mutex, fm);

    if (det->stream_category == 0 && det->pcnt > 3 && det->duration > 0) {
        float datarate = (float)det->flowsize_bytes/det->duration;
        if (datarate <= 10000)
            det->stream_category = 4;
        else 
            det->stream_category = 5;
    }
   
    det->headers_length = ss->headers_size;

    int ecn_16b_blocks = 0;
    int i;
    for (i = 0; i < 8; i++) {
        if (CHECK_BIT(ss->ce_16b_blocks,0))
            ecn_16b_blocks++;
        else
            break;
    }
    det->ecn_packets = ss->ce_packets + 65535 * ecn_16b_blocks;

    return 0;
}

//-------------------------------------------------------------------------------------
void* Analyzer::outputStreamStat (SrcDst sd, struct StreamStats *ss, vector<StreamDetails*> *streamDetailsVector, bool is_tcp, GSMap* gsmap, pthread_mutex_t *gsmap_mutex, FileManager *fm)
{   
    StreamDetails* sdetails = new StreamDetails();

    sdetails->src_ip.ipv4 = sd.m_srcip;
    sdetails->dst_ip.ipv4 = sd.m_dstip;
    sdetails->src_port = sd.m_srcport;
    sdetails->dst_port = sd.m_dstport;

    Analyzer::stat(ss, sdetails, is_tcp, gsmap, gsmap_mutex, fm);
   
    if (sdetails->pcnt == 0 && sdetails->acnt < 2) {
        delete sdetails;
        return 0;
    }
    streamDetailsVector->push_back(sdetails);

    return 0;
}
//-------------------------------------------------------------------------------------
void* Analyzer::outputStreamStat6 (SrcDst6 sd, struct StreamStats *ss, vector<StreamDetails*> *streamDetailsVector, bool is_tcp)
{   
   /* if (ss->iatVector.empty() || ss->plList.empty())    
        return 0;
    StreamDetails* sdetails = new StreamDetails();

    sdetails->src_ip.ipv6 = new struct in6_addr();
    memcpy(sdetails->src_ip.ipv6, &sd.m_srcip, sizeof(struct in6_addr));
    sdetails->dst_ip.ipv6 = new struct in6_addr();
    memcpy(sdetails->dst_ip.ipv6, &sd.m_dstip, sizeof(struct in6_addr));
    sdetails->src_port = sd.m_srcport;
    sdetails->dst_port = sd.m_dstport;

    Analyzer::stat(ss, sdetails, is_tcp);
    streamDetailsVector->push_back(sdetails);*/

    return 0;
}
//-------------------------------------------------------------------------------------
void* Analyzer::analyzeStreamMap (void* param)
{
    thread_param* tp = (thread_param*) param;
    SMap::iterator itCur;
    SMap::iterator itEnd;
    vector<StreamDetails*> streamDetailsVector;
    streamDetailsVector.reserve(STREAM_DETAILS_VECTOR_SIZE);
    do {
        pthread_mutex_lock(tp->map_mutex);
        itCur = tp->map.smap->begin();
        itEnd = tp->map.smap->end();
        if (itCur == itEnd) {
            pthread_mutex_unlock(tp->map_mutex);
            break;
        }
        SrcDst s_d(itCur->first);
        StreamStats* ss_cur = itCur->second;
        itCur = tp->map.smap->erase(itCur);
        pthread_mutex_unlock(tp->map_mutex);

        in_addr_t src_ip, dst_ip;
        uint16_t src_p, dst_p;
        src_ip = s_d.m_srcip;
        src_p = s_d.m_srcport;
        dst_ip = s_d.m_dstip;
        dst_p = s_d.m_dstport;
       
        pthread_mutex_lock(&tp->update_progress_mutex);
        tp->cur_maplength = tp->map.smap->size();
        pthread_cond_signal(&tp->update_progress);
        pthread_mutex_unlock(&tp->update_progress_mutex);
            
        if (ss_cur->malformed){
            if (tp->is_tcp)
                (*(tp->malformed_tcp))++;
            else
                (*(tp->malformed_udp))++;
        } else  
            outputStreamStat(s_d, ss_cur, &streamDetailsVector, tp->is_tcp, tp->gsmap, tp->gsmap_mutex, tp->fm);
        if (streamDetailsVector.size() >= STREAM_DETAILS_VECTOR_SIZE) 
            tp->dbm->write(&streamDetailsVector, tp->is_tcp, 4);
        if (ss_cur)
           delete ss_cur;
    } while (itCur != itEnd);
    tp->dbm->write(&streamDetailsVector, tp->is_tcp, 4);
    tp->sl.lock();
    int i = 0;
    int j = 0;
    while(i < 100) {
        if (tp->thread_complete[i] == 0) {
            tp->thread_complete[i] = 1;
        } else
            i++;
    }
    tp->sl.unlock();
    return 0;
}
//-------------------------------------------------------------------------------------
void* Analyzer::analyzeStreamMap6 (void* param)
{
   /* thread_param* tp = (thread_param*) param;
    SMap6::iterator itCur;
    SMap6::iterator itEnd;
    vector<StreamDetails*> streamDetailsVector;
    streamDetailsVector.reserve(STREAM_DETAILS_VECTOR_SIZE);
   
    do {
        struct in6_addr src_ip, dst_ip;
        uint16_t src_p, dst_p;
        
        pthread_mutex_lock(tp->map_mutex);
        itCur = tp->map.smap6->begin();
        itEnd = tp->map.smap6->end();
        pthread_mutex_lock(&tp->progress_mutex);
        tp->cur_maplength = tp->map.smap6->size();
        pthread_mutex_unlock(&tp->progress_mutex);
        pthread_cond_signal(&tp->update_progress);
        if (itCur == itEnd) {
            pthread_mutex_unlock(tp->map_mutex);
            break;
        } 
        SrcDst6 s_d(itCur->first);
        src_ip = s_d.m_srcip;
        src_p = s_d.m_srcport;
        dst_ip = s_d.m_dstip;
        dst_p = s_d.m_dstport;
        StreamStats* ss_cur = itCur->second;
        itCur = tp->map.smap6->erase(itCur);
        pthread_mutex_unlock(tp->map_mutex);
        if (ss_cur->malformed)
            (*(tp->malformed))++;
        else
            outputStreamStat6(s_d, ss_cur, &streamDetailsVector, tp->is_tcp, tp->stat);
        if (streamDetailsVector.size() >= STREAM_DETAILS_VECTOR_SIZE) {
            tp->dbm->write(&streamDetailsVector, tp->is_tcp, 6);
        }
        if (stat == IAT) {
            list<uint32_t>* l = (list<uint32_t>*)ss_cur->mList;
            delete l;
        } else {
            list<uint16_t>* l = (list<uint16_t>*)ss_cur->mList;
            delete l;
        }
        delete ss_cur;
    } while (itCur != itEnd);  
    tp->dbm->write(&streamDetailsVector, tp->is_tcp, 6);*/
    return 0;
}
