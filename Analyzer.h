/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#ifndef ANALYZER_H
#define ANALYZER_H

#include "structs.h"
#include "DbManager.h"
#include "FileManager.h"
#include "KMeansCluster.h"
#include <vector>

#define NVALUE 50
#define STREAM_DETAILS_VECTOR_SIZE 100000

# define percentile(p, n) (round(float(p)/100*float(n) + float(1)/2))

using namespace std;

struct thread_param {
    pthread_mutex_t update_progress_mutex;
    pthread_mutex_t *map_mutex;
    pthread_mutex_t *gsmap_mutex;
    pthread_cond_t update_progress;
    pthread_mutexattr_t errorcheck;
    Spinlock sl;
    GSMap *gsmap;
    union {
        SMap *smap;
        SMap6 *smap6;
    } map;
    DbManager *dbm;
    FileManager *fm;
    int num_threads;
    uint64_t cur_maplength;
    bool is_tcp;
    int stat;
    int* malformed_tcp;
    int* malformed_udp;
    bool thread_complete[100];

    thread_param(){
        cur_maplength = 0;
        pthread_mutexattr_init(&errorcheck);
        pthread_mutex_init(&update_progress_mutex,&errorcheck);
        pthread_cond_init(&update_progress, NULL);
        is_tcp = true;
        stat = 0;
        malformed_tcp = 0;
        malformed_udp = 0;
        int i = 0;
        for (i = 0; i < 100; i++)
            thread_complete[i] = 0;
    }

};

class Analyzer
{
public:

    Analyzer();
    ~Analyzer();
    static void* iatStat(StreamStats* ss, StreamDetails* det, bool is_tcp, GSMap* gsmap, pthread_mutex_t *gsmap_mutex, FileManager *fm);
    static void* plStat(StreamStats* ss, StreamDetails* det, bool is_tcp);
    static void* rttStat(StreamStats* ss, StreamDetails* det);
    static void* streamCategoryStat(StreamStats* ss, list<KMeansCluster> *clusters, StreamDetails *det, uint16_t segment_size, uint64_t nr_values, uint64_t mss_pkts, GSMap* gsmap, pthread_mutex_t *gsmap_mutex, FileManager *fm); 
    static void* stat(StreamStats *ss, StreamDetails *det, bool is_tcp, GSMap* gsmap, pthread_mutex_t *gsmap_mutex, FileManager *fm);
    static void* outputStreamStat (SrcDst sd, struct StreamStats *ss, std::vector<StreamDetails*> *streamDetailsVector, bool is_tcp, GSMap* gsmap, pthread_mutex_t *gsmap_mutex, FileManager* fm);
    static void* outputStreamStat6 (SrcDst6 sd, struct StreamStats *ss, std::vector<StreamDetails*> *streamDetailsVector, bool is_tcp);
    static void* analyzeStreamMap (void* param);
    static void* analyzeStreamMap6 (void* param);

};

#endif /* ANALYZER_H */
