#!/bin/bash
declare -i total_connections total_data
declare -i result_connections result_data result_percentage_data result_percentage_connections 
declare -i result

select_conn_query="select count (*) from stats "
select_data_query="select sum(flowsize_bytes) from stats "
delete_query="delete from stats "
where=""
dbname="t${1}"

TCP_ports_http=(80 8080)
TCP_UDP_ports_https=(443)
TCP_UDP_port_dns=(53)
TCP_UDP_well_known_ports=(0-1023)
TCP_ports_xbox=(1900 3390 3074 3776 3932 5555 7777)
UDP_ports_xbox_=(80 1900 3390 3074 3776 3932 5555 7777)
TCP_ports_games=(1030 1024 1200 2300-2400 2346-2348 3658 3659 3724 5001-5010 6000 6112-6119 6073 6667 6881-6999 7000-10300 10400-10499 13505 17010-17012 18210 18215 \
	20100-20112 20500-20510 20803 20809 23456 27000-27015 27020-27039 27650-27666 27888 27900 27910 27950 27952 27960 27965 28000 28001 28910-28915 28960 30900-30999 32768-65535)
UDP_ports_games=(1030 1024 1200 2300-2400 2346-2348 3658 3659 5120-5300 6000 6112-6119 6500 6667 6073 6667 7000-10300 10400-10499 13505 18210 18215 18699-28900 \
	28910-28915 28960 30900-30999 32768-65535)
TCP_ports_rdp=(3389 5500 5632 5800-5900) 
UDP_ports_rdp=(3389 5500 5632 5500 5800 5900)
TCP_UDP_ports_p2p=(411 6346 6347) # DC++ uses ports 411 and 1025-32000, but other applications use ports in the same range. Need a better strategy to filter it (maybe considering )
TCP_UDP_ports_media_streaming=(1755 3689 6970-7000 7007)
TCP_ports_voip=(1034 1035 2644 8000 9900 9901 10200 11675)
UDP_ports_voip=(1034 1035 2074-2076 2644 5061 6801 8000 9900 9901 10000-20000)

function printInfo()
{
	result_percentage_connections=0
	result_percentage_data=0
	result_percentage_connections=$result_connections*100/total_connections
	result_percentage_data=$result_data*100/$total_data
	echo $1 $result_connections "connections ("$result_percentage_connections" %), " $result_data" bytes transferred ("$result_percentage_data" %)"
	result_connections=0
	result_data=0
	
}
function getConnections()
{
	result=$result_connections
	current_select_query=$select_conn_query$where
	result_connections=$(sqlite3 $dbname "${current_select_query}")
	result_connections=$result_connections+$result
	
}
function getData()
{
	result=$result_data
	current_select_query=$select_data_query$where
	result_data=$(sqlite3 $dbname "${current_select_query}")
	result_data=$result_data+$result
	
}
function buildWhereQuery()
{
	if [[ $item == *-* ]]; 	then
   		lowerlimit=$(echo ${item} | awk -F'-' '{print $1}')
  		upperlimit=$(echo ${item} | awk -F'-' '{print $2}')
  		where="where (src_port>="$lowerlimit" and src_port<="$upperlimit") or (dst_port>="$lowerlimit" and dst_port<="$upperlimit");"
  	else
  		where="where src_port="${item}" or dst_port="${item}";"
	fi
}
function TCP_stats()
{
	echo "TCP_stats"
	for item in ${TCP_ports_http[*]}; do 
		buildWhereQuery $item
		getConnections  
		getData 
	done
	printInfo "HTTP: "

	for item in ${TCP_UDP_ports_https[*]}; do 
		buildWhereQuery $item
		getConnections  
		getData 
	done
	printInfo "HTTPS: "

	for item in ${TCP_UDP_port_dns[*]}; do 
		buildWhereQuery $item
		getConnections  
		getData 
	done
	printInfo "DNS: "

	for item in ${TCP_ports_games[*]}; do 
		buildWhereQuery $item
		getConnections  
		getData 
	done

}

function UDP_stats()
{
	for item in ${TCP_UDP_ports_https[*]}; do 
		buildWhereQuery $item
		getConnections  
		getData 
	done
	printInfo "HTTPS: "

	for item in ${TCP_UDP_port_dns[*]}; do 
		buildWhereQuery $item
		getConnections  
		getData 
	done
	printInfo "DNS: "

	

	where=";"
	getConnections
	getData
	printInfo "Other: "
}
read -n 6 dataset <<< "$1"
echo $dataset

cp $1 t${1}
# if [[ $dbname == *tcp* ]]; then
# 	#delete connections that have no syn and no fin flag and last for shorter than 1800 sec
# 	sqlite3 $dbname "delete from stats where completed == 0 and duration < 1800;"
# fi
echo "dbname" $dbname
total_connections=$(sqlite3 ${dbname} "select count(*) from stats;")
echo "Total connections: "$total_connections
total_data=$(sqlite3 ${dbname} "select sum(flowsize_bytes) from stats;")
echo "Total bytes of data: "$total_data 

#if [[ $dbname == *tcp* ]]; then
#   	TCP_stats
#elif [[ $dbname == *udp* ]]; then
#  	UDP_stats
#fi

#cp $1 t${1}

# if [[ $dbname == *tcp* ]]; then
# 	#delete connections that have no syn and no fin flag and last for shorter than 1800 sec
# 	sqlite3 $dbname "delete from stats where completed == 0 and duration < 1800;"
# fi
line=$dataset
for i in $(seq 0 7); do
	where="where stream_category == $i;"
	getConnections
	getData
	line=$line" "${result_connections}" "${result_data}
	printInfo "Stream category $i: "
	line=$line" "${result_percentage_connections}" "${result_percentage_data}
	
done
echo ${line} >> stream_classes.txt

if [[ $dbname == *tcp* ]]; then
     # info about repeating connections - same src_port (80)
	repeating_conn_src80="select count (*), sum (nr_streams), sum(total_bytes) from (select src_ip, dst_ip, src_port, count(*) as nr_streams, sum (flowsize_bytes) as total_bytes from stats group by src_ip, dst_ip, src_port having src_port = 80 and nr_streams >= 2 and rst=0);"
	src80_conn_bytes=$(sqlite3 ${dbname} "${repeating_conn_src80}")
	result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $2}')
	result_data=$(echo ${src80_conn_bytes} | awk -F'|' '{print $3}') 
	printInfo "Repeating connections source port 80: "
	result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $1}')
	echo "unique src_ip, dst_ip, src_port groups: " $result_connections
	# info about repeating connections - same dst_port (80)

	repeating_conn_dst80="select count (*), sum (nr_streams), sum(total_bytes) from (select src_ip, dst_ip, dst_port, count(*) as nr_streams, sum (flowsize_bytes) as total_bytes from stats group by src_ip, dst_ip, dst_port having dst_port = 80 and nr_streams >= 2 and rst=0);"
	dst80_conn_bytes=$(sqlite3 ${dbname} "${repeating_conn_dst80}")
	result_connections=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $2}')
	result_data=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $3}') 
	printInfo "Repeating connections destination port 80: "
	result_connections=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $1}')
	echo "unique src_ip, dst_ip, dst_port groups: " $result_connections

	# Dash streaming 
	linedash=$dataset
	sqlite3 $dbname "update stats set stream_category = 10 where stream_category = 3 or stream_category = 2;"
	repeating_conn_src80="select count (*), sum (nr_streams), sum(total_bytes), min(tot_duration) from (select src_ip, dst_ip, src_port, sum(stream_category/10) as cat, count(*) as nr_streams, sum(pkttrain_lr_det/pkttrain_lr_det) as nr_lr_samples, sum (pkttrain_lr_det) as tot_det, sum(pkttrain_lr_cor) as tot_cor, max(timestamp_start_sec+duration)-min(timestamp_start_sec) as tot_duration, sum(flowsize_bytes) as total_bytes from stats group by src_ip, dst_ip, src_port having src_port = 80 and nr_streams >= 2 and cat == nr_streams and tot_cor/nr_lr_samples > 0 and tot_det/nr_lr_samples > 0);"
	src80_conn_bytes=$(sqlite3 ${dbname} "${repeating_conn_src80}")
	result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $2}')
	result_data=$(echo ${src80_conn_bytes} | awk -F'|' '{print $3}') 
	linedash=${linedash}" "${result_data}
	printInfo "Repeating connections: DASH: "
	linedash=${linedash}" "${result_percentage_data}
	result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $1}')
	min_duration=$(echo ${src80_conn_bytes} | awk -F'|' '{print $4}')
	echo "min duration: " $min_duration
	echo "unique src_ip, dst_ip, src_port groups: " $result_connections

 	sqlite3 $dbname "update stats set stream_category = 10 where stream_category = 3 or stream_category = 2;"
    repeating_conn_src80="select count (*), sum (nr_streams), sum(total_bytes), min(tot_duration) from (select src_ip, dst_ip, src_port, sum(stream_category/10) as cat, count(*) as nr_streams, sum(pkttrain_lr_det/pkttrain_lr_det) as nr_lr_samples, sum (pkttrain_lr_det) as tot_det, sum(pkttrain_lr_cor) as tot_cor, max(timestamp_start_sec+duration)-min(timestamp_start_sec) as tot_duration, sum(flowsize_bytes) as total_bytes from stats group by src_ip, dst_ip, src_port having src_port = 80 and nr_streams >= 2 and cat == nr_streams and tot_cor/nr_lr_samples > 0 and tot_det/nr_lr_samples > 0); where tot_duration >= 80"
    src80_conn_bytes=$(sqlite3 ${dbname} "${repeating_conn_src80}")
    result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $2}')
    result_data=$(echo ${src80_conn_bytes} | awk -F'|' '{print $3}')
    linedash=${linedash}" "${result_data}
    printInfo "Repeating connections: DASH (lasting over 80 sec): "
    linedash=${linedash}" "${result_percentage_data}
    result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $1}')
    min_duration=$(echo ${src80_conn_bytes} | awk -F'|' '{print $4}')
    echo "min duration: " $min_duration
    echo "unique src_ip, dst_ip, src_port groups: " $result_connections

echo ${linedash} >> dash.txt


elif [[ $dbname == *udp* ]]; then
	# info about repeating connections - same src_port (80)
	repeating_conn_src80="select count (*), sum (nr_streams), sum(total_bytes) from (select src_ip, dst_ip, src_port, count(*) as nr_streams, sum (flowsize_bytes) as total_bytes from stats group by src_ip, dst_ip, src_port having src_port = 80 and nr_streams >= 2);"
	src80_conn_bytes=$(sqlite3 ${dbname} "${repeating_conn_src80}")
	result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $2}')
	result_data=$(echo ${src80_conn_bytes} | awk -F'|' '{print $3}') 
	printInfo "Repeating connections source port 80: "
	result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $1}')
	echo "unique src_ip, dst_ip, src_port groups: " $result_connections
	# info about repeating connections - same dst_port (80)
	repeating_conn_dst80="select count (*), sum (nr_streams), sum(total_bytes) from (select src_ip, dst_ip, dst_port, count(*) as nr_streams, sum (flowsize_bytes) as total_bytes from stats group by src_ip, dst_ip, dst_port having dst_port = 80 and nr_streams >= 2) where tot_duration >= 80s;"
	dst80_conn_bytes=$(sqlite3 ${dbname} "${repeating_conn_dst80}")
	result_connections=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $2}')
	result_data=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $3}') 
	printInfo "Repeating connections destination port 80: "
	result_connections=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $1}')
	echo "unique src_ip, dst_ip, dst_port groups: " $result_connections

fi
# result_connections=0
# if [[ $dbname == *tcp* ]]; then
# 	echo "ONLY COMPLETED STREAMS"
# 	total_connections=$(sqlite3 ${dbname} "select count(*) from stats where completed != 0 or duration >= 1800;")
# 	echo "Total connections: "$total_connections
# 	total_data=$(sqlite3 ${dbname} "select sum(flowsize_bytes) from stats where completed != 0 or duration >= 1800;")
# 	echo "Total bytes of data: "$total_data 


# 	for i in $(seq 0 5); do
# 		where="where stream_category == $i and (completed != 0 or duration >= 1800);"
# 		getConnections
# 		getData
# 		printInfo "Stream category $i: "
# 	done
# 	 # info about repeating connections - same src_port (80)
# 	repeating_conn_src80="select count (*), sum (nr_streams), sum(total_bytes) from (select src_ip, dst_ip, src_port, count(*) as nr_streams, sum (flowsize_bytes) as total_bytes from stats group by src_ip, dst_ip, src_port having src_port = 80 and nr_streams >= 2 and rst=0 and (completed != 0 or duration >= 1800));"
# 	src80_conn_bytes=$(sqlite3 ${dbname} "${repeating_conn_src80}")
# 	result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $2}')
# 	result_data=$(echo ${src80_conn_bytes} | awk -F'|' '{print $3}') 
# 	printInfo "Repeating connections source port 80: "
# 	result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $1}')
# 	echo "unique src_ip, dst_ip, src_port groups: " $result_connections
# 	# info about repeating connections - same dst_port (80)
# 	repeating_conn_dst80="select count (*), sum (nr_streams), sum(total_bytes) from (select src_ip, dst_ip, dst_port, count(*) as nr_streams, sum (flowsize_bytes) as total_bytes from stats group by src_ip, dst_ip, dst_port having dst_port = 80 and nr_streams >= 2 and rst=0 and (completed != 0 or duration >= 1800));"
# 	dst80_conn_bytes=$(sqlite3 ${dbname} "${repeating_conn_dst80}")
# 	result_connections=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $2}')
# 	result_data=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $3}') 
# 	printInfo "Repeating connections destination port 80: "
# 	result_connections=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $1}')
# 	echo "unique src_ip, dst_ip, dst_port groups: " $result_connections


# 	repeating_conn_src80="select count (*), sum (nr_streams), sum(total_bytes) from (select src_ip, dst_ip, src_port, count(*) as nr_streams, sum (flowsize_bytes) as total_bytes from stats group by src_ip, dst_ip, src_port, timestamp_start_sec having src_port = 80 and nr_streams >= 2 and rst=0 and (completed != 0 or duration >= 1800));"
# 	src80_conn_bytes=$(sqlite3 ${dbname} "${repeating_conn_src80}")
# 	result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $2}')
# 	result_data=$(echo ${src80_conn_bytes} | awk -F'|' '{print $3}') 
# 	printInfo "Repeating connections, same start time, source port 80: "
# 	result_connections=$(echo ${src80_conn_bytes} | awk -F'|' '{print $1}')
# 	echo "unique src_ip, dst_ip, src_port groups: " $result_connections
# 	# info about repeating connections - same dst_port (80)
# 	repeating_conn_dst80="select count (*), sum (nr_streams), sum(total_bytes) from (select src_ip, dst_ip, dst_port, count(*) as nr_streams, sum (flowsize_bytes) as total_bytes from stats group by src_ip, dst_ip, dst_port, timestamp_start_sec having dst_port = 80 and nr_streams >= 2 and rst=0 and (completed != 0 or duration >= 1800));"
# 	dst80_conn_bytes=$(sqlite3 ${dbname} "${repeating_conn_dst80}")
# 	result_connections=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $2}')
# 	result_data=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $3}') 
# 	printInfo "Repeating connections, same start time, destination port 80: "
# 	result_connections=$(echo ${dst80_conn_bytes} | awk -F'|' '{print $1}')
# 	echo "unique src_ip, dst_ip, dst_port groups: " $result_connections
# fi

#current_delete_query=$delete_query"where id not in (select id from stats where src_port == 80 or dst_port == 80);"
#sqlite3 $dbname "$current_delete_query"
#current_delete_query=$delete_query"where id in (select distinct src_ip,dst_ip from stats);"
#sqlite3 $dbname "$current_delete_query"






