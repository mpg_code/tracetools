/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#include <sstream>
#include  <sys/socket.h>
#include  <arpa/inet.h>
#include "DbManager.h"

DbManager::DbManager(char* filename) {
    tcptable_entries = 0;
    udptable_entries = 0;
    pthread_mutexattr_init(&errorcheck);
    pthread_mutex_init(&databaseTCP_mutex, &errorcheck);
    pthread_mutex_init(&databaseTCPIP6_mutex, &errorcheck);
    pthread_mutex_init(&databaseUDP_mutex, &errorcheck);
    pthread_mutex_init(&databaseUDPIP6_mutex, &errorcheck);
    databaseTCP = NULL, databaseUDP = NULL;
    dbFilename = filename;
    connectDatabase();
}
//-------------------------------------------------------------------------------------
DbManager::~DbManager() {
    sqlite3_close(databaseTCP);
  //  sqlite3_close(databaseTCPIP6);
    sqlite3_close(databaseUDP);
  //  sqlite3_close(databaseUDPIP6);
    sqlite3_shutdown();
}
//-------------------------------------------------------------------------------------
void DbManager::connectDatabase() {
    if (sqlite3_initialize() != SQLITE_OK) {
        cerr << "Failed to init database engine" << endl;
    }
    
    string dbFilenameTCP = dbFilename;
    dbFilenameTCP = dbFilenameTCP + "tcp.db";
    string dbFilenameUDP = dbFilename;
    dbFilenameUDP = dbFilenameUDP + "udp.db";
/*    string dbFilenameTCPIP6 = dbFilename + "tcp_ip6.db";
    string dbFilenameUDPIP6 = dbFilename + "udp_ip6.db";*/

   
    cout << "Output: "  << dbFilenameUDP << ", " 
                     //   << dbFilenameUDPIP6 << ", " 
                        << dbFilenameTCP << ", " 
                     //   << dbFilenameTCPIP6 << ", " 
                        << endl;
    
    if (sqlite3_open(dbFilenameTCP.c_str(), &databaseTCP) != SQLITE_OK) {
        cerr << "Failed to open output TCP database" << endl;
        // TODO throw exception
    }
    if (sqlite3_open(dbFilenameUDP.c_str(), &databaseUDP) != SQLITE_OK) {
        cerr << "Failed to open output TCP database" << endl;
        // TODO throw exception
    }
  /*  if (sqlite3_open(dbFilenameTCPIP6.c_str(), &databaseTCPIP6) != SQLITE_OK) {
        cerr << "Failed to open output TCP database" << endl;
        // TODO throw exception
    }
    if (sqlite3_open(dbFilenameUDPIP6.c_str(), &databaseUDPIP6) != SQLITE_OK) {
        cerr << "Failed to open output TCP database" << endl;
        // TODO throw exception
    }*/



    char* errorMessage = new char(200);

    int res = 0;
    res = sqlite3_exec(databaseTCP, "PRAGMA synchronous=OFF", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseTCP, "PRAGMA count_changes=OFF", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseTCP, "PRAGMA journal_mode=MEMORY", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseTCP, "PRAGMA temp_store=MEMORY", NULL, NULL, &errorMessage);
    if (res != 0) cout << errorMessage << endl;

   /* res = sqlite3_exec(databaseTCPIP6, "PRAGMA synchronous=OFF", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseTCPIP6, "PRAGMA count_changes=OFF", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseTCPIP6, "PRAGMA journal_mode=MEMORY", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseTCPIP6, "PRAGMA temp_store=MEMORY", NULL, NULL, &errorMessage);
    if (res != 0) cout << errorMessage << endl;*/
    
  
   // const char* create_stmt_tcp6;
   // const char* create_stmt_udp6;


    /*
    Stream categories:
    0 - undefined
    1 - greedy (MSS-sized packets with a smaller packet in the end of the TCP connection)
    2 - bursty (smaller packets in the end of each packet train)
    3 - bursty (bursts with smaller packets below MSS in between)
    4 - thin (low data rate)
    5 - greedy (high data rate)

    */
    const char* create_stmt_tcp = "CREATE TABLE IF NOT EXISTS stats \
            (id INTEGER PRIMARY KEY, \
            src_ip INTEGER, \
            src_port INTEGER, \
            dst_ip INTEGER, \
            dst_port INTEGER, \
            completed INTEGER, \
            rst INTEGER, \
            zero_win INTEGER, \
            ecn_packets INTEGER, \
            duration FLOAT, \
            timestamp_start_sec INTEGER, \
            timestamp_start_usec INTEGER, \
            pktcount INTEGER, \
            ackcount INTEGER, \
            rtt_ms_avg INTEGER, \
            iat_ms_kmeans_clusters, \
            iat_us_median INTEGER, \
            payload_median INTEGER, \
            pkttrains_nr INTEGER, \
            pkttrain_len_avg INTEGER, \
            pkttrain_len_median INTEGER, \
            pkttrain_iat_us_avg INTEGER, \
            pkttrain_iat_us_median INTEGER, \
            pkttrain_int_us_avg INTEGER, \
            pkttrain_int_us_median INTEGER, \
            pkttrain_lr_det DOUBLE, \
            pkttrain_lr_cor DOUBLE, \
            headers_length INTEGER, \
            flowsize_bytes INTEGER, \
            stream_category INTEGER);";
        const char*  create_stmt_udp = "CREATE TABLE IF NOT EXISTS stats \
            (id INTEGER PRIMARY KEY, \
            src_ip INTEGER, \
            src_port INTEGER, \
            dst_ip INTEGER, \
            dst_port INTEGER, \
            ecn_packets INTEGER, \
            duration FLOAT, \
            timestamp_start_sec INTEGER, \
            timestamp_start_usec INTEGER, \
            pktcount INTEGER, \
            rtt_ms_avg INTEGER, \
            iat_ms_kmeans_clusters, \
            iat_us_median INTEGER, \
            payload_median INTEGER, \
            pkttrains_nr INTEGER, \
            pkttrain_len_avg INTEGER, \
            pkttrain_len_median INTEGER, \
            pkttrain_iat_us_avg INTEGER, \
            pkttrain_iat_us_median INTEGER, \
            pkttrain_int_us_avg INTEGER, \
            pkttrain_int_us_median INTEGER, \
            pkttrain_lr_det DOUBLE, \
            pkttrain_lr_cor DOUBLE, \
            headers_length INTEGER, \
            flowsize_bytes INTEGER, \
            stream_category INTEGER);";
     
      
    sqlite3_stmt* pStmt_tcp;
    int dbe = sqlite3_prepare_v2(databaseTCP, create_stmt_tcp, -1, &pStmt_tcp, 0);
    if(dbe != SQLITE_OK) {
        cerr << "failed to prepare database query to insert tables:" << dbe << endl;
        // TODO throw exception return 1;
    }
    sqlite3_step(pStmt_tcp);
    sqlite3_finalize(pStmt_tcp);

    /*sqlite3_stmt* pStmt_tcp6;
    dbe = sqlite3_prepare_v2(databaseTCPIP6, create_stmt_tcp6, -1, &pStmt_tcp6, 0);
    if(dbe != SQLITE_OK) {
        cerr << "failed to prepare database query to insert tables:" << dbe << endl;
        // TODO throw exception return 1;
    }
    sqlite3_step(pStmt_tcp6);
    sqlite3_finalize(pStmt_tcp6);*/

    res = sqlite3_exec(databaseUDP, "PRAGMA synchronous=OFF", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseUDP, "PRAGMA count_changes=OFF", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseUDP, "PRAGMA journal_mode=MEMORY", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseUDP, "PRAGMA temp_store=MEMORY", NULL, NULL, &errorMessage);
    if (res != 0) cout << errorMessage << endl;
    
  /*  res = sqlite3_exec(databaseUDPIP6, "PRAGMA synchronous=OFF", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseUDPIP6, "PRAGMA count_changes=OFF", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseUDPIP6, "PRAGMA journal_mode=MEMORY", NULL, NULL, &errorMessage); 
    if (res != 0) cerr << errorMessage << endl;
    res = sqlite3_exec(databaseUDPIP6, "PRAGMA temp_store=MEMORY", NULL, NULL, &errorMessage);
    if (res != 0) cout << errorMessage << endl;*/
   
    sqlite3_stmt* pStmt_udp;
    dbe = sqlite3_prepare_v2(databaseUDP, create_stmt_udp, -1, &pStmt_udp, 0);
    if(dbe != SQLITE_OK) {
        cerr << "failed to prepare database query to insert tables:" << dbe << endl;
        // TODO throw exception return 1;
    }
    sqlite3_step(pStmt_udp);
    sqlite3_finalize(pStmt_udp);

   /* sqlite3_stmt* pStmt_udp6;
    dbe = sqlite3_prepare_v2(databaseUDPIP6, create_stmt_udp6, -1, &pStmt_udp6, 0);
    if(dbe != SQLITE_OK) {
        cerr << "failed to prepare database query to insert tables:" << dbe << endl;
        // TODO throw exception return 1;
    }
    sqlite3_step(pStmt_udp6);
    sqlite3_finalize(pStmt_udp6);*/


}
//-------------------------------------------------------------------------------------
void DbManager::writeDatabase(StreamDetails *sdetails, bool is_tcp, int ipv)
{
    sqlite3 **database;

   // if (ipv == 4) {
        if (is_tcp)
            database = &databaseTCP;
        else
            database = &databaseUDP;
   /* } else {
        if (is_tcp)
            database = &databaseTCPIP6;
        else
            database = &databaseUDPIP6;
    }*/

    if (*database == NULL)
        return;

    stringstream out;
    out << "INSERT INTO stats VALUES (NULL,";
    if (ipv == 4) {
        out <<  sdetails->src_ip.ipv4 << ", " << sdetails->src_port << ", " <<
                sdetails->dst_ip.ipv4 << ", " << sdetails->dst_port << ", ";
    } else {
        char src_ip[INET6_ADDRSTRLEN];
        char dst_ip[INET6_ADDRSTRLEN];
        struct in6_addr src;
        memcpy(&src, sdetails->src_ip.ipv6, sizeof(struct in6_addr)); 
        struct in6_addr dst;
        memcpy(&dst, sdetails->dst_ip.ipv6, sizeof(struct in6_addr)); 

        inet_ntop(AF_INET6, &src, src_ip, INET6_ADDRSTRLEN);
        inet_ntop(AF_INET6, &dst, dst_ip, INET6_ADDRSTRLEN);
       
        out <<  "'" << src_ip << "'" << ", " << sdetails->src_port << ", " <<
                "'" << dst_ip << "'" << ", " << sdetails->dst_port << ", ";   
    }
    if (is_tcp) {
        out <<  (int) sdetails->complete << ", " << 
                (int) sdetails->rst << ", " << 
                (int) sdetails->zero_win << ", ";
    }

    out <<  sdetails->ecn_packets << ", " 
        <<  sdetails->duration << ", "
        <<  sdetails->start_time.tv_sec << ", "
        <<  sdetails->start_time.tv_usec << ", "
        <<  sdetails->pcnt << ", ";
        if (is_tcp)
            out <<  sdetails->acnt << ", ";
    out <<  sdetails->rtt_average << ", ";
    if (sdetails->iat_kmeans_clusters.length() > 1)
        out << "'" << sdetails->iat_kmeans_clusters << "', ";
    else
        out << "'none'" << ", ";
    out <<  sdetails->iat_median << ", "
        <<  sdetails->payload_median << ", ";

  
    out <<  sdetails->packet_trains_nr << ", " 
        <<  sdetails->packet_train_length_avg << ", "
        <<  sdetails->packet_train_length_median << ", "
        <<  sdetails->packet_train_iat_avg << ", "
        <<  sdetails->packet_train_iat_median << ", "
        <<  sdetails->packet_train_int_avg << ", "
        <<  sdetails->packet_train_int_median << ", "
        <<  sdetails->packet_train_lr_det << ", "
        <<  sdetails->packet_train_lr_cor << ", ";

    out <<  (int)sdetails->headers_length << ", " 
        <<  sdetails->flowsize_bytes << ", "
        <<  (int)sdetails->stream_category << ");";

    sqlite3_stmt* pStmt;
    
   // cout << "query" << out.str() << endl;
    
    int dbe = sqlite3_prepare_v2(*database, out.str().c_str(), -1, &pStmt, 0);
    if(dbe != SQLITE_OK) {
        cerr << "failed to prepare database query while inserting data: " << dbe << endl;
        if (is_tcp)
            cout << "TCP" << endl;
        else
            cout << "UDP" << endl;
        cerr << out.str() << endl;
    }
    else 
        sqlite3_step(pStmt);

    sqlite3_finalize(pStmt);
    if (ipv == 6) {
        delete sdetails->src_ip.ipv6;
        delete sdetails->dst_ip.ipv6;
    }
    delete sdetails;
}
//-------------------------------------------------------------------------------------
void DbManager::write(vector<StreamDetails*> *sdetailsVector, bool is_tcp, int ipv)
{
    pthread_mutex_t* outfile_mutex;
 //   if (ipv == 4) {
        if (is_tcp)
            outfile_mutex = &databaseTCP_mutex;
        else
            outfile_mutex = &databaseUDP_mutex;
   /* } else {
        if (is_tcp)
            outfile_mutex = &databaseTCPIP6_mutex;
        else
            outfile_mutex = &databaseUDPIP6_mutex;
    }*/

    pthread_mutex_lock(outfile_mutex);
    for (vector<StreamDetails*>::iterator it = sdetailsVector->begin(); it != sdetailsVector->end(); ++it) {
        writeDatabase(*it, is_tcp, ipv);
        if (is_tcp)
            tcptable_entries++;
        else
            udptable_entries++;        
    }

    pthread_mutex_unlock(outfile_mutex);
    sdetailsVector->clear();
}


 