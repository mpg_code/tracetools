#ifndef IP_DEFS_H
#define IP_DEFS_H

#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <net/ethernet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

//defines for the packet type code in an ETHERNET header
#define ETHER_TYPE_IP (0x0800)
#define ETHER_TYPE_8021Q (0x8100)

#define	IP_DF 0x4000			/* dont fragment flag */
#define	IP_MF 0x2000			/* more fragments flag */
#define	IP_OFFMASK 0x1fff		/* mask for fragmenting bits */

#define MAX_FO 65528
#define MAX_PLSZ_UDP 65535
#define MAX_PLSZ_TCP 65495
#define MAX_IPV6_JUMBO_UDP 4294967295

#if defined(__linux__)
#  define IPHDR struct iphdr
#  define IHL(iphdr)    iphdr->ihl
#  define IPV(iphdr)    iphdr->version
#  define IP_LEN(iphdr) ntohs(iphdr->tot_len)
#  define PCOL(iphdr)   iphdr->protocol
#  define SADDR(iphdr)  iphdr->saddr
#  define DADDR(iphdr)  iphdr->daddr
#  define ID(iphhdr)	iphdr->id
#  define IP_OFF(iphdr) ntohs(iphdr->frag_off)

#  define UDPHDR struct udphdr
#  define UDP_SPORT(udphdr) ntohs(udphdr->source)
#  define UDP_DPORT(udphdr) ntohs(udphdr->dest)
#  define UDP_LEN(udphdr)   ntohs(udphdr->len)

#  define TCPHDR struct tcphdr
#  define DOFF(tcphdr)  tcphdr->doff
#  define SEQ(tcphdr)   ntohl(tcphdr->seq)
#  define ACK(tcphdr)   ntohl(tcphdr->ack_seq)
#  define SPORT(tcphdr) ntohs(tcphdr->source)
#  define DPORT(tcphdr) ntohs(tcphdr->dest)

#  define IS_ACK(tcphdr) (tcphdr->ack)
#  define IS_SYN(tcphdr) (tcphdr->syn)
#  define IS_FIN(tcphdr) (tcphdr->fin)
#  define IS_RST(tcphdr) (tcphdr->rst)
#  define IS_PSH(tcphdr) (tcphdr->psh)
#  define IS_URG(tcphdr) (tcphdr->urg)

#else
#  define IPHDR struct ip
#  define IHL(iphdr)    iphdr->ip_hl
#  define IPV(iphdr)    iphdr->ip_v
#  define IP_LEN(iphdr) ntohs(iphdr->ip_len)
#  define PCOL(iphdr)   iphdr->ip_p
#  define SADDR(iphdr)  iphdr->ip_src.s_addr
#  define DADDR(iphdr)  iphdr->ip_dst.s_addr
#  define ID(iphhdr)	iphdr->ip_id
#  define IP_OFF(iphdr) ntohs(iphdr->ip_off)

#  define UDPHDR struct udphdr
#  define UDP_SPORT(udphdr) ntohs(udphdr->uh_sport)
#  define UDP_DPORT(udphdr) ntohs(udphdr->uh_dport)
#  define UDP_LEN(udphdr)   ntohs(udphdr->uh_ulen)

#  define TCPHDR struct tcphdr
#  define DOFF(tcphdr)  tcphdr->th_off
#  define SEQ(tcphdr)   ntohl(tcphdr->th_seq)
#  define ACK(tcphdr)   ntohl(tcphdr->th_ack)
#  define SPORT(tcphdr) ntohs(tcphdr->th_sport)
#  define DPORT(tcphdr) ntohs(tcphdr->th_dport)

#  define IS_ACK(tcphdr) (tcphdr->th_flags & TH_ACK)
#  define IS_SYN(tcphdr) (tcphdr->th_flags & TH_SYN)
#  define IS_FIN(tcphdr) (tcphdr->th_flags & TH_FIN)
#  define IS_RST(tcphdr) (tcphdr->th_flags & TH_RST)
#  define IS_PSH(tcphdr) (tcphdr->th_flags & TH_PUSH)
#  define IS_URG(tcphdr) (tcphdr->th_flags & TH_URG)
#endif

inline uint16_t payload_len_tcp( const IPHDR* const iphdr, const TCPHDR* const tcphdr )
{
	return IP_LEN(iphdr) - IHL(iphdr)*4 - DOFF(tcphdr)*4;
}

inline uint16_t payload_len_udp(const UDPHDR* const udphdr )
{
	// udp header is always 8 bytes   
	return UDP_LEN(udphdr) - 8;
}

inline bool MF(const IPHDR* const iphdr)
{
	return (IP_OFF(iphdr)) & IP_MF;
}
inline uint16_t FO(const IPHDR* const iphdr)
{
	return (IP_OFF(iphdr)) & IP_OFFMASK;
}

#endif /* IP_DEFS_H */

