/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#include <sstream>
#include <sys/stat.h>
#include "FileManager.h"

//-------------------------------------------------------------------------------------
bool compare_nocase (string first, string second)
{
    unsigned int i = 0;
    while ((i < first.length()) && (i < second.length()))
    {
        if (tolower(first[i]) < tolower(second[i])) return true;
        else if (tolower(first[i]) > tolower(second[i])) return false;
        ++i;
    }
    if (first.length() < second.length()) return true;
    else return false;
}
//-------------------------------------------------------------------------------------
FileManager::FileManager(bool isfile, char* name, bool rm)
{
    if (isfile)
        readFile(name);
    else
        readDir(name);
    remove_files = rm;
    if (remove_files) {
        pthread_attr_t attrs;
        pthread_attr_init(&attrs);
        pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_JOINABLE);   
        int res = pthread_create(&tp.thread_id,&attrs,&FileManager::removeFile,(void*)&tp);
        if (res != 0) {
            cerr << "Error while creating thread, exiting..." << endl;
            exit(1);
        }
    }
}
//-------------------------------------------------------------------------------------
FileManager::~FileManager()
{
    if (remove_files) {
        pthread_mutex_lock(&tp.cond_mutex);
        tp.lastfile = true;
        tp.file_to_remove = filename;
        pthread_cond_signal(&tp.remove_file);
        pthread_mutex_unlock(&tp.cond_mutex);
        int res = pthread_join(tp.thread_id, NULL);
        if (res != ESRCH && res != 0)
            cerr << res << ": error joining thread " << endl;
    }
}
//-------------------------------------------------------------------------------------
void FileManager::readDir(char* name)
{    
    DIR *cur_dir;
    struct dirent *entry;
    if( (cur_dir = opendir(name)) != NULL) {
        while((entry = readdir(cur_dir)) != NULL) {
            if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                string filename = name;
                filename = filename + "/" + entry->d_name;
                if (0 == filename.compare (filename.length() - 5, 5, ".pcap")) 
                    filenames.push_back(filename);
            }
        }
        closedir(cur_dir);
    } else {
        cout << "'"<< name << "'" << ": no such directory." << endl;
        exit(1);
    }
    if (filenames.size() == 0){
        cout << "No *.pcap files in this directory." << endl;
        exit(1);
    }
    filenames.sort(compare_nocase);
    for (list<string>::iterator it = filenames.begin(); it != filenames.end(); ++it) 
        cout << *it << endl;
}
//-------------------------------------------------------------------------------------
void FileManager::readFile(char* name)
{
    filenames.push_back(string(name));
} 
//-------------------------------------------------------------------------------------
bool FileManager::isEmpty()
{
    if (filenames.size() == 0)
        return true;
    else
        return false;
}
//-------------------------------------------------------------------------------------
int FileManager::filesLeft()
{
    return filenames.size();
}
//-------------------------------------------------------------------------------------
string FileManager::nextFile(pcap_t **file)
{  
    if (*file) {
        pcap_close(*file);
        if (remove_files) {
            tp.file_to_remove = filename;
            pthread_mutex_lock(&tp.mutex);
            pthread_mutex_lock(&tp.cond_mutex);
            pthread_mutex_unlock(&tp.mutex);
            pthread_cond_signal(&tp.remove_file);
            pthread_mutex_unlock(&tp.cond_mutex);
        }
    }
    filename = filenames.front();
   
    filenames.pop_front();
      
    char errbuf[PCAP_ERRBUF_SIZE];
    *file = pcap_open_offline(filename.c_str(), errbuf);
    
    if (!*file) {
        cerr << "Error reading file: " << errbuf << endl;
        exit(1);
    }

    stringstream str_filename;
    int i = filename.length();
    while (filename[--i] != '/')
        str_filename << filename[i];

    string result = "";
    for (i = str_filename.str().length()-1; i >= 0; i--)
        result = result + str_filename.str()[i];

    return result;
}
//-------------------------------------------------------------------------------------
void* FileManager::removeFile(void* param)
{
    fm_thread_param* tp = (fm_thread_param*) param;
    while (true) {
        pthread_mutex_lock(&tp->cond_mutex);
        pthread_cond_wait(&tp->remove_file, &tp->cond_mutex);
        pthread_mutex_lock(&tp->mutex);
        pthread_mutex_unlock(&tp->cond_mutex);
        remove(tp->file_to_remove.c_str());
        tp->file_to_remove = "";
        if (tp->lastfile) {
            remove(tp->file_to_remove.c_str());
            pthread_mutex_unlock(&tp->mutex);
            return 0;
        }
        pthread_mutex_unlock(&tp->mutex);
    }
}
