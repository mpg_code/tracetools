/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#include <stdio.h>
#include <sstream>
#include <sys/stat.h>
#include <unistd.h>
#include <algorithm>
#include "FileManager.h"
#include "ProgressBar.h"
#include "ipdefs.h"
#include <math.h>

int getIPoffset(int linktype) {
    if (linktype == DLT_EN10MB)
        return ETHER_HDR_LEN;
    else if (linktype == DLT_RAW)
        return 0;
    else if (linktype == DLT_C_HDLC || linktype == DLT_NULL)
        return 4;
    else {
        cout << "Linktype: " << linktype << "not implemented yet. Feel free to do that :) or report" << endl;
        exit(1);
    }
}
//-------------------------------------------------------------------------------------
int getIndex(in_addr_t ip, int fragments) {
    if (fragments == 1)
        return 0;
    stringstream ip_str;
    ip_str << ip;
    int key;
    stringstream key_str;
    int i;
    for (i = 2; i >= 0; i--)
        key_str << ip_str.str()[i];
   
    key_str >> key;
    int step = 1000/fragments;
    int index = key/step;
    if (index >= fragments)
        index = fragments - 1;
  
    return index;
}
//-------------------------------------------------------------------------------------
void splitIPv6 (FileManager *fm) 
{
    stringstream dirname;
    dirname << "output/ipv6";
    mkdir(dirname.str().c_str(),0777);  

  
    int nrFiles = fm->filesLeft();
    cout << "Splitting: " << nrFiles << " file(s)" << endl;

    FILE* f;
    pcap_dumper_t* dumper;
   
    uint64_t ipv6_packets = 0;
    
    ProgressBar::update(0);
    pcap_t *file = NULL;
    do {
        IPHDR* iphdr;
        string cur_filename = fm->nextFile(&file);

        stringstream filename;
        filename << "output/ipv6/" << cur_filename << "-ipv6.pcap";
            f = fopen(filename.str().c_str(), "ab");
            dumper = NULL;
            dumper = pcap_dump_fopen(file, f);

        if (!file) {
            cerr << "error while opening file." << endl;
            exit(1);
        }
        int linktype = pcap_datalink(file);
                
        const u_char *data;
        struct pcap_pkthdr h;

        data = pcap_next(file, &h);
        if (data == NULL) {
            cerr << "unable to read file" << endl;
            continue;
        }

        int iphdroffset = getIPoffset(linktype);
        iphdr = (IPHDR*) (data + iphdroffset);
        do {
            int ipv = IPV(iphdr);
            if (ipv == 6) 
                pcap_dump ((u_char *)dumper, &h, (unsigned char *)data);
            data = pcap_next(file, &h); 
        } while (data != NULL);
        pcap_dump_close(dumper);
       
        ProgressBar::update((nrFiles - fm->filesLeft())*100/nrFiles);
    } while (!fm->isEmpty());
    if (file)
        pcap_close(file);

}
//-------------------------------------------------------------------------------------
void splitAll (int fragments, FileManager *fm)
{
    int i;  
    for (i = 0; i < fragments; i++) {
        stringstream dirname;
        dirname << "output/" << i;
        mkdir(dirname.str().c_str(),0777);  
    } 
    stringstream dirname;
    dirname << "output/ipv6";
    mkdir(dirname.str().c_str(),0777);  

  
    int nrFiles = fm->filesLeft();
    cout << "Splitting: " << nrFiles << " file(s)" << endl;

    FILE* f[fragments+1];
    pcap_dumper_t* dumper[fragments+1];
   
    uint64_t ipv6_packets = 0, tcp_packets = 0, udp_packets = 0, other_packets = 0;
    
    ProgressBar::update(0);

    pcap_t *file = NULL;
    do {
        IPHDR* iphdr;
        
        string cur_filename = fm->nextFile(&file);

        for (i = 0; i < fragments; i++) {
            stringstream filename;
            filename << "output/" << i << "/" << cur_filename.c_str() << "-" << i << ".pcap";
            f[i] = fopen(filename.str().c_str(), "ab");
            dumper[i] = NULL;
            dumper[i] = pcap_dump_fopen(file, f[i]);
        }
        // ipv6
        stringstream filename;
        filename << "output/ipv6/" << cur_filename.c_str() << "-ipv6.pcap";
            f[i] = fopen(filename.str().c_str(), "ab");
            dumper[i] = NULL;
            dumper[i] = pcap_dump_fopen(file, f[i]);

        if (!file) {
            cerr << "error while opening file." << endl;
            exit(1);
        }
        int linktype = pcap_datalink(file);
        int iphdroffset = getIPoffset(linktype);
       
        const u_char *data;
        struct pcap_pkthdr h;
        data = pcap_next(file, &h);
        if (data == NULL) {
            cerr << "unable to read file" << endl;
            continue; 
        } 
        do {
            iphdr = (IPHDR*) (data + iphdroffset);
            int ipv = IPV(iphdr);
            in_addr_t src_ip = SADDR(iphdr);
            if (ipv == 6) {
                pcap_dump ((u_char *)dumper[fragments], &h, (unsigned char *)data);
                ipv6_packets++;
            } else {
                u_char protocol = PCOL(iphdr);
                if (protocol == IPPROTO_TCP || protocol == IPPROTO_UDP) {
                    int index = getIndex(src_ip, fragments);
                    if (dumper[index] == NULL) {
                        cout << "unable to open dumper file" << endl;
                        exit(1);
                    }
                    pcap_dump ((u_char *)dumper[index], &h, (unsigned char *)data);
                } else
                    other_packets++;
            }
            data = pcap_next(file, &h);
        } while (data != NULL);

        for (i = 0; i < fragments; i++) 
            pcap_dump_close(dumper[i]);
        pcap_dump_close(dumper[fragments]);
       
        ProgressBar::update((nrFiles - fm->filesLeft())*100/nrFiles);
    } while (!fm->isEmpty());
    if (file)
        pcap_close(file);
}
//-------------------------------------------------------------------------------------
void filterStreams (FileManager *fm, in_addr_t src, in_addr_t dst)
{
    int nrFiles = fm->filesLeft();
    cout << "Filtering: " << nrFiles << " file(s)" << endl;

    FILE* f;
    pcap_dumper_t* dumper;
   
    uint64_t tcp_packets = 0, udp_packets = 0;
    
    ProgressBar::update(0);

    pcap_t *file = NULL;
    do {
        IPHDR* iphdr;
        string cur_filename = fm->nextFile(&file);
        stringstream filename;
        filename << "output/" << cur_filename.c_str() << "-src-" << src << "_dst-" << dst << ".pcap";
        f = fopen(filename.str().c_str(), "ab");
        dumper = NULL;
        dumper = pcap_dump_fopen(file, f);
      
        if (!file) {
            cerr << "error while opening file." << endl;
            exit(1);
        }
        int linktype = pcap_datalink(file);
        int iphdroffset = getIPoffset(linktype);
                
        const u_char *data;
        struct pcap_pkthdr h;

        do {
            data = pcap_next(file, &h);
            if (data == NULL) 
                continue;            
            iphdr = (IPHDR*) (data + iphdroffset);
            int ipv = IPV(iphdr);
            if (ipv == 4) {
                u_char protocol = PCOL(iphdr);
                if (protocol != IPPROTO_TCP && protocol != IPPROTO_UDP)
                    continue;

                if (src != 0) {
                    in_addr_t src_ip = SADDR(iphdr);
                    if (src_ip != src)
                        continue;
                } 
                if (dst != 0) {
                    in_addr_t dst_ip = DADDR(iphdr);
                    if (dst_ip != dst)
                        continue;
                }
                if (dumper == NULL) {
                    cout << "unable to open dumper file" << endl;
                    exit(1);
                }
                pcap_dump ((u_char *)dumper, &h, (unsigned char *)data);
                 
            }
        } while (data != NULL);
        pcap_dump_close(dumper);
        ProgressBar::update((nrFiles - fm->filesLeft())*100/nrFiles);
    } while (!fm->isEmpty());
    if (file)
        pcap_close(file);
}
//-------------------------------------------------------------------------------------
void usage(int argc, char* argv[])
{
    printf("Usage: %s  -f|-d <name> [ options ]\n", argv[0]);
    printf("OPTIONS:\n");
    printf("-f : single file, -d: directory containing trace files\n");
    printf("-c <nr>: number of fragments.\n");
    printf("ipv6-only: only filter out ipv6 packets (fragments option is not allowed in this case)\n");
    printf("-src <address>: filter out packets from this source\n");
    printf("-dst <address>: filter out packets to this destination\n");
    exit(1); 
}
//-------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{   
    if (argc < 4) 
        usage(argc, argv);

    struct timeval start, end, total;
    gettimeofday(&start, NULL);
      
    bool single_file;
    bool ipv6_only = false;
    bool remove_origin_files = false;
    int fragments = 1;
    in_addr_t src = 0;
    in_addr_t dst = 0;
    char *path;

    if (argc >= 5){
        int i = 1;
        while (i < argc) {
            if (strcmp(argv[i], "-f") == 0 && argc >= i+2) {
                single_file = true;
                path = argv[i+1];
                i += 2;
            } else if (strcmp(argv[i], "-d") == 0 && argc >= i+2) {
                single_file = false;
                path = argv[i+1];
                i += 2;
            } else if (strcmp(argv[i], "-c") == 0 && argc >= i+2) {
                fragments = atoi(argv[i+1]);
                i += 2;
            } else if (strcmp(argv[i], "ipv6-only") == 0) {
                ipv6_only = true;
                i++;
            } else if (strcmp(argv[i], "-src") == 0 && argc >= i+2) {
                src = atoi(argv[i+1]);
                i += 2;
            } else if (strcmp(argv[i], "-dst") == 0 && argc >= i+2) {
                dst = atoi(argv[i+1]);
                i += 2;
            } else if (strcmp(argv[i], "-rm") == 0) {
                remove_origin_files = true;
                i++;
            } else
                usage(argc, argv);
        }
    } else
        usage(argc, argv);

  
    if (fragments > 100) {
        cout << "Maximum number of fragments is 100." << endl;
        exit(1);
    }
        
    mkdir("output",0777); 
    FileManager* fm = new FileManager(single_file, path, remove_origin_files);

    if (ipv6_only)
        splitIPv6(fm);
    else if (src != 0 || dst != 0)
        filterStreams(fm, src, dst);
    else
        splitAll(fragments, fm);
    
    delete fm;
    return 0;
}


