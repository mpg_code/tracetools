/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#include <iostream>
#include <cstring>
#include <cstdio>
#include "ProgressBar.h"

using namespace std;

//-------------------------------------------------------------------------------------
ProgressBar::ProgressBar()
{
}
//-------------------------------------------------------------------------------------
ProgressBar::~ProgressBar()
{
}
//-------------------------------------------------------------------------------------
void* ProgressBar::update(int percentage) 
{
    char buffer[101];
    if (percentage < 0) percentage = 0;
    if (percentage > 100) percentage = 100;
    buffer[percentage] = '>';
        memset(buffer, '=', percentage);
    memset(buffer+percentage+1, ' ', 100-percentage);
    buffer[100] = 0;

    fprintf(stdout, "[%s] %d %%\r", buffer, percentage);
    fflush(stdout);
    if (percentage == 100)
        cout << endl;
    return 0;
}