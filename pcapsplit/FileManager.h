/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <dirent.h>
#include <pcap.h>
#include <string.h>
#include <iostream>
#include <list>
#include <pthread.h>

using namespace std;

struct fm_thread_param {
    pthread_t thread_id;
    pthread_cond_t remove_file;
    pthread_mutex_t mutex, cond_mutex;
    string file_to_remove;
    bool lastfile;
    fm_thread_param() {
        pthread_mutexattr_t errorcheck;
        pthread_mutexattr_init(&errorcheck);
        pthread_mutex_init(&mutex, &errorcheck);
        pthread_mutex_init(&cond_mutex, &errorcheck);
        pthread_cond_init(&remove_file, NULL);
        lastfile = false;
    }
};

class FileManager
{
public:
    FileManager(bool isfile, char* name, bool rm);
    ~FileManager();
    bool isEmpty();
    string nextFile(pcap_t **file);
    int filesLeft();
    static void* removeFile(void* param);
    fm_thread_param tp;
    string filename;

   
   
protected:
    void readDir(char* name);
    void readFile(char* name);
    list<string> filenames;
    bool remove_files;
};

#endif /* FILEMANAGER_H */
