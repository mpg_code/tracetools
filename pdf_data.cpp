#include <stdio.h>
#include <sys/stat.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <sqlite3.h>
#include <dirent.h>
#include <list>
#include <stdint.h>
#include <algorithm>
#include <cstring>
using namespace std;
bool compare_nocase (string first, string second)
{
    unsigned int i = 0;
    while ((i < first.length()) && (i < second.length()))
    {
        if (tolower(first[i]) < tolower(second[i])) return true;
        else if (tolower(first[i]) > tolower(second[i])) return false;
        ++i;
    }
    if (first.length() < second.length()) return true;
    else return false;
}
int main(int argc, char* argv[])
{  
    if (argc < 2) {
    	printf("Usage: %s <input dir> \n", argv[0]);
    	return 1;
    }

    list<string> filenames;
    DIR *cur_dir;
    struct dirent *entry;
    if( (cur_dir = opendir(argv[1])) != NULL) {
        while((entry = readdir(cur_dir)) != NULL) {
            if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                string filename = argv[1];
                filename = filename + "/" + entry->d_name;
                if (0 == filename.compare (filename.length() - 6, 6, "tcp.db"))  
                    filenames.push_back(filename);
            }
        }
        closedir(cur_dir);
    } else {
        cout << "'"<< argv[1] << "'" << ": no such directory." << endl;
        exit(1);
    }
    if (filenames.size() == 0){
        cout << "No  files in this directory." << endl;
        return 1;
    }
    filenames.sort(compare_nocase);
    uint64_t pdf_perc[101];
    vector<uint32_t> *pktcount_vector = new vector<uint32_t>;
    pktcount_vector->reserve(100000000);
    int i;
    for (i = 0; i < 101; i++) 
        pdf_perc[i] = 0;

    for (list<string>::iterator filenames_it = filenames.begin(); filenames_it != filenames.end(); ++filenames_it) {

        sqlite3 *db;
        sqlite3_stmt *res;
        
        
        cout << *filenames_it << endl;

     
        if (sqlite3_open((*filenames_it).c_str(), &db)) {
            sqlite3_close(db);
            cerr << "Can't open database: " << sqlite3_errmsg(db) << endl;
            return(1);
        }

        if (sqlite3_prepare_v2(db, "SELECT pktcount, pktcount*100/(ackcount+pktcount) FROM stats", -1, &res, NULL) != SQLITE_OK) {
            sqlite3_close(db);
            cerr << "Can't retrieve data: " << sqlite3_errmsg(db) << endl;
            return(1);
        }

        cout << "Reading data..." << endl;

        while (sqlite3_step(res) == SQLITE_ROW) {
            uint32_t pktcount;
            uint32_t data_percentage;
            pktcount = sqlite3_column_int(res, 0);  
            data_percentage = sqlite3_column_int(res, 1);
            pktcount_vector->push_back(pktcount);
            pdf_perc[data_percentage]++;
        }
       
    }

    sort(pktcount_vector->begin(), pktcount_vector->end());          

        ofstream f_pdf_perc ("pdf_perc.txt");
        if (f_pdf_perc.is_open()) {
            for (i = 0; i < 101; i++)    
                f_pdf_perc << i << " " << pdf_perc[i] << endl;
            f_pdf_perc.close();
        } else cout << "Unable to open output file" << endl;
        

        ofstream f_ecdf_perc ("ecdf_perc.txt");
        if (f_ecdf_perc.is_open()) {
            uint64_t total = 0;
            for (i = 0; i < 101; i++) {   
                total += pdf_perc[i];    
                f_ecdf_perc << i << " " << total << endl;
            }
            f_ecdf_perc.close();
        } else cout << "Unable to open output file" << endl;

        ofstream f_pktcount_pdf("pktcount_pdf.txt");
        uint32_t value = pktcount_vector->front();
        uint64_t count = 0;
        if (f_pktcount_pdf.is_open()) {
            for (vector<uint32_t>::iterator it = pktcount_vector->begin(); it != pktcount_vector->end(); ++it) {
                if (*it == value)
                    count++;
                else {
                    f_pktcount_pdf << value << " " << count << endl;
                    value = *it;
                    count = 1;
                }

            }
            f_pktcount_pdf << value << " " << count << endl;
            f_pktcount_pdf.close();
        } else cout << "Unable to open output file" << endl;
    
    return 0;
}
