/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#ifndef STRUCTS_H
#define STRUCTS_H

//#define DEBUG 1
//#define BWPLOT 1
//#define NTEST 1
//#define LINREG 1
//#define MSS_PSH_TEST 1


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <pcap.h> 
#include <errno.h>
#include <time.h> 
#include <sys/types.h>
#include <assert.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h> /* includes net/ethernet.h */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unordered_map>
#include <list>
#include <iostream>
#include <utility>
#include <cstdlib>
#include <fstream>
#include <dirent.h>
#include <deque>
#include <pthread.h>
#include <unistd.h>
#include <vector>
#include <algorithm>
#include "ipdefs.h"
#include "ListElem.h"
#include "LinReg.h"

#ifdef __APPLE__
    #include <libkern/OSAtomic.h>
#endif

#ifdef NTEST
    static uint64_t ntest_res[100];
    static pthread_mutex_t ntest_mutex;
#endif

#ifdef MSS_PSH_TEST
    static uint64_t msspsh_conn[101][101];
    static uint64_t msspsh_data[101][101];
  //  static uint64_t max_pl_conn[101][101];
    //static uint64_t max_pl_data[101][101];
    static pthread_mutex_t msspsh_mutex;
#endif

#define MAX_UINT16  65535
#define MAX_UINT32  4294967295

#define IAT     0
#define PL      1
#define PCKTLEN 2

#define CONSEQ_SEG 6

#define REASSEMBLE_BUFSZ 60 // max size of a TCP header

#define CHECK_BIT(var,pos) ((var) & (1 << (pos)))
#define SET_BIT(var,pos) ((var) |= (1 << (pos)))
#define CLEAR_BIT(var,pos) ((var) &= ~(1 << (pos))) 

#define CHECK_BIT(var,pos) ((var) & (1 << (pos)))
#define SET_BIT(var,pos) ((var) |= (1 << (pos)))

using namespace std;


# define timesub_timeval2ms(cur_time, prev_time, result)     \
    struct timeval res; \
    timersub(&cur_time, &prev_time, &res); \
    result = (uint32_t)((res.tv_sec * 1000) + (res.tv_usec / 1000));    \

# define timesub_timeval2us(cur_time, prev_time, result)     \
    struct timeval res; \
    timersub(&cur_time, &prev_time, &res); \
    result = (uint32_t)(res.tv_sec * 1000000 + res.tv_usec);    \


struct TimeDiff {
    uint16_t tv_sec;
    suseconds_t tv_usec;
    TimeDiff(){
        tv_sec = 0;
        tv_usec = 0;
    }
};

# define timediff_sub(cur_time, init_time, result)     \
    bzero(&result, sizeof(struct TimeDiff));        \
    struct timeval res_sub;     \
    bzero(&res_sub, sizeof(struct timeval)); \
    timersub(&cur_time, &init_time, &res_sub);     \
    result.tv_sec = (uint16_t)res_sub.tv_sec;     \
    result.tv_usec = res_sub.tv_usec;       \

# define timediff_add(a, b, result)     \
    struct timeval res_add;     \
    bzero(&res_add, sizeof(struct timeval));        \
    res_add.tv_sec = (uint32_t)b.tv_sec;     \
    res_add.tv_usec = b.tv_usec;        \
    bzero(&result, sizeof(struct timeval));      \
    timeradd(&a, &res_add, &result);     \
    
   
typedef long stats; 

struct SrcDst_IP {
public:
    SrcDst_IP(in_addr_t srcip, in_addr_t dstip)
    : m_srcip(srcip)
    , m_dstip(dstip)
    {}
    in_addr_t m_srcip, m_dstip;
       
    bool operator<(const SrcDst_IP& rhs) const {
        if (m_srcip != rhs.m_srcip)
            return m_srcip < rhs.m_srcip;
        return m_dstip < rhs.m_dstip;
    }
    bool operator==(const SrcDst_IP& rhs) const {
        return m_srcip == rhs.m_srcip &&
                m_dstip == rhs.m_dstip;
    }
};

struct SrcDst {
public:
    SrcDst(in_addr_t srcip, uint16_t srcport, in_addr_t dstip, uint16_t dstport)
    : m_srcip(srcip)
    , m_srcport(srcport)
    , m_dstip(dstip)
    , m_dstport(dstport)
    {}
    in_addr_t m_srcip, m_dstip;
    uint16_t m_srcport, m_dstport;
    
    bool operator<(const SrcDst& rhs) const {
        if (m_srcip != rhs.m_srcip)
            return m_srcip < rhs.m_srcip;
        if (m_dstip != rhs.m_dstip)
            return m_dstip < rhs.m_dstip;
        if (m_srcport != rhs.m_srcport)
            return m_srcport < rhs.m_srcport;
        return m_dstport < rhs.m_dstport;
    }
    
    bool operator==(const SrcDst &rhs) const
    {
        return m_srcip == rhs.m_srcip &&
                m_dstip == rhs.m_dstip &&
                m_srcport == rhs.m_srcport &&
                m_dstport == rhs.m_dstport;
    }
};

struct SrcDst6 {
public:
    SrcDst6(struct in6_addr srcip, uint16_t srcport, struct in6_addr dstip, uint16_t dstport)
    : m_srcip(srcip)
    , m_srcport(srcport)
    , m_dstip(dstip)
    , m_dstport(dstport)
    {}
    struct in6_addr m_srcip;
    struct in6_addr m_dstip;
    uint16_t m_srcport, m_dstport;
    
    bool operator<(const SrcDst6& rhs) const {
        int i;
        for (i = 0; i < 4; i++) {
            if (m_srcip.ADDR6[i])
                return m_srcip.ADDR6[i] < rhs.m_srcip.ADDR6[i];
        }
        for (i = 0; i < 4; i++) {
            if (m_dstip.ADDR6[i] != rhs.m_dstip.ADDR6[i])
                return m_dstip.ADDR6[i] < rhs.m_dstip.ADDR6[i];
        }
        if (m_srcport != rhs.m_srcport)
            return m_srcport < rhs.m_srcport;
        return m_dstport < rhs.m_dstport;
    }
    
    bool operator==(const SrcDst6 &rhs) const
    {
        return m_srcip.ADDR6[0] == rhs.m_srcip.ADDR6[0] &&
                m_srcip.ADDR6[1] == rhs.m_srcip.ADDR6[1] &&
                m_srcip.ADDR6[2] == rhs.m_srcip.ADDR6[2] &&
                m_srcip.ADDR6[3] == rhs.m_srcip.ADDR6[3] &&
                m_dstip.ADDR6[0] == rhs.m_dstip.ADDR6[0] &&
                m_dstip.ADDR6[1] == rhs.m_dstip.ADDR6[1] &&
                m_dstip.ADDR6[2] == rhs.m_dstip.ADDR6[2] &&
                m_dstip.ADDR6[3] == rhs.m_dstip.ADDR6[3] &&
                m_srcport == rhs.m_srcport &&
                m_dstport == rhs.m_dstport;
    }
};

template <>
struct std::equal_to<SrcDst> : public unary_function<SrcDst, bool>
{
    bool operator()(const SrcDst& x, const SrcDst& y) const
    {
        return x.m_srcip == y.m_srcip &&
                x.m_dstip == y.m_dstip &&
                x.m_srcport == y.m_srcport &&
                x.m_dstport == y.m_dstport;
    }
};

// from boost
template <class T>
inline void hash_combine(size_t & seed, const T & v)
{
  static std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

struct SrcDstIPHasher {
    std::size_t operator() (const SrcDst_IP& t) const {
        size_t h = 0;
        ::hash_combine(h, t.m_srcip);
        ::hash_combine(h, t.m_dstip);
        return h;
    }
};

struct SrcDstHasher {
    std::size_t operator() (const SrcDst& t) const {
        size_t h = 0;
        ::hash_combine(h, t.m_srcip);
        ::hash_combine(h, t.m_dstip);
        ::hash_combine(h, t.m_srcport);
        ::hash_combine(h, t.m_dstport);
        return h;
    }
};

struct SrcDst6Hasher {
    std::size_t operator() (const SrcDst6& t) const {
        size_t h = 0;
        ::hash_combine(h, t.m_srcip.ADDR6[0]);
        ::hash_combine(h, t.m_srcip.ADDR6[1]);
        ::hash_combine(h, t.m_srcip.ADDR6[2]);
        ::hash_combine(h, t.m_srcip.ADDR6[3]);
        ::hash_combine(h, t.m_dstip.ADDR6[0]);
        ::hash_combine(h, t.m_dstip.ADDR6[1]);
        ::hash_combine(h, t.m_dstip.ADDR6[2]);
        ::hash_combine(h, t.m_dstip.ADDR6[3]);
        ::hash_combine(h, t.m_srcport);
        ::hash_combine(h, t.m_dstport);
        return h;
    }
};

class GStreamStats {
public:
    LinearRegression lr;

    struct timeval start_time;
    struct timeval end_time;
    uint16_t nr_streams;
    uint64_t flowsize_bytes;

    uint16_t srcport;
    bool http:1;
    bool same_srcport:1;
    bool onoff_only:1;

    
    GStreamStats() {
        bzero(&start_time, sizeof(struct timeval));
        bzero(&start_time, sizeof(struct timeval));
        srcport = 0;
        http = 0;
        same_srcport = 1;
        onoff_only = 1;
        nr_streams = 0;
        flowsize_bytes = 0;
    };
    ~GStreamStats(){}
};

class StreamStats {
public:
    vector<ListElem*> iatVector;
    list<uint16_t> plList;

    struct timeval init_time;
    TimeDiff prev_time;

    uint8_t malformed:1;
    uint8_t slow_start:1;
    uint8_t conseq_segments:6;
    uint8_t ce_16b_blocks;
    uint16_t ce_packets;
    uint8_t headers_size;

    StreamStats() {
        bzero(&init_time, sizeof(struct timeval));
        bzero(&prev_time, sizeof(struct TimeDiff));
        slow_start = 0;
        malformed = 0;
        conseq_segments = 0;
        ce_16b_blocks = 0; // 1 block = 65535
        ce_packets = 0;
        headers_size = 0;
    };
    ~StreamStats(){
        for (vector<ListElem*>::iterator it = iatVector.begin(); it != iatVector.end(); ++it)
            delete *it;
    }
};

class StreamStatsTCP: public StreamStats {
public:
  //  list<uint32_t> ackIatList;
    bool first:1;
    bool syn:1;
    bool synack:1;
    bool fin:1;
    bool rst:1;
    bool rtt_sample:1;
    bool keepalive:1;
    bool retr:1;
    bool zero_win:1;
    uint32_t ackcount;

   // TimeDiff prev_time_ack;

    uint32_t init_seq; 
    struct seq_len {uint32_t seq; uint32_t ack; uint16_t pl_sz;} prev_packet;

    StreamStatsTCP() {
        first = 0;
        syn = 0;
        synack = 0;
        fin = 0;
        rst = 0;
        rtt_sample = 0;
        keepalive = 0;
        retr = 0;
        zero_win = 0;
        ackcount = 0;

        init_seq = MAX_UINT32;
        prev_packet.seq = 0;
        prev_packet.ack = 0;
        prev_packet.pl_sz = 0;
    };
    ~StreamStatsTCP(){}
};

typedef unordered_map<SrcDst, StreamStats*, SrcDstHasher> SMap;
typedef unordered_map<SrcDst6, StreamStats*, SrcDst6Hasher> SMap6;
typedef unordered_map<SrcDst_IP, GStreamStats*, SrcDstIPHasher> GSMap;

typedef union IPAddr
{
    uint32_t ipv4;
    in6_addr* ipv6;
} IPAddr;

class StreamDetails {
public:
    IPAddr src_ip;
    IPAddr dst_ip;
    uint16_t src_port;
    uint64_t dst_port;
    uint32_t pcnt;
    uint32_t acnt;
    float duration;
    uint64_t ecn_packets;
    uint8_t complete:2; 
    uint8_t rst:1;
    uint8_t zero_win:1;
    uint8_t stream_category:3;

    
    uint8_t headers_length;
    struct timeval start_time;

    uint16_t rtt_average;
    string iat_kmeans_clusters;
    uint32_t iat_median;
    uint16_t payload_avg;
    uint16_t payload_median;
    uint16_t packet_trains_nr;
    uint32_t packet_train_length_avg;
    uint32_t packet_train_length_median; 
    uint32_t packet_train_iat_avg;
    uint32_t packet_train_iat_median;
    uint32_t packet_train_int_avg;
    uint32_t packet_train_int_median;
    double packet_train_lr_det;
    double packet_train_lr_cor;
    uint64_t flowsize_bytes;

    StreamDetails(){
        src_ip.ipv4 = 0, dst_ip.ipv4 = 0;
        src_port = 0, dst_port = 0;
        ecn_packets = 0;
        complete = 0;
        rst = 0;
        stream_category = 0;
        pcnt = 0;
        acnt = 0;
        duration = 0;
        headers_length = 0;
        rtt_average = 0;
        iat_median = 0;
        payload_avg = 0;
        payload_median = 0;
        packet_trains_nr = 0;
        packet_train_length_avg = 0;
        packet_train_length_median = 0;
        packet_train_iat_avg = 0;
        packet_train_iat_median = 0;
        packet_train_int_avg = 0;
        packet_train_int_median = 0;
        packet_train_lr_det = 0;
        packet_train_lr_cor = 0;
        flowsize_bytes = 0;
        zero_win = 0;
        stream_category = 0;

    }
    ~StreamDetails(){}
};
class GStreamDetails {
public:
    IPAddr src_ip;
    IPAddr dst_ip;
    float duration;
   
    uint16_t packet_trains_nr;
    uint32_t packet_train_length_avg;
    uint32_t packet_train_length_median; 
    uint32_t packet_train_iat_avg;
    uint32_t packet_train_iat_median;
    uint32_t packet_train_int_avg;
    uint32_t packet_train_int_median;
    double packet_train_lr_det;
    double packet_train_lr_cor;
    uint64_t flowsize_bytes;

    GStreamDetails(){
        src_ip.ipv4 = 0;
        dst_ip.ipv4 = 0;
        duration = 0;
        packet_trains_nr = 0;
        packet_train_length_avg = 0;
        packet_train_length_median = 0;
        packet_train_iat_avg = 0;
        packet_train_iat_median = 0;
        packet_train_int_avg = 0;
        packet_train_int_median = 0;
        packet_train_lr_det = 0;
        packet_train_lr_cor = 0;
        flowsize_bytes = 0;
    }
    ~GStreamDetails(){}
};



class Buffer {
public: 
    char buffer[REASSEMBLE_BUFSZ];
    Buffer(){
        bzero(buffer, REASSEMBLE_BUFSZ);
    }
    ~Buffer(){}
};

class ReassembledPacket {
public:
    Buffer b;
    vector<int> rcvbt;
    int tdl;
    TimeDiff diff_ts;

    ReassembledPacket(){
        tdl = 0;
    }
    ~ReassembledPacket(){}
      //  if (b != 0)
        //    delete b;
   // }
    void setBit(int bit){
        int index = bit/32;
        while (rcvbt.size() <= index){
            int newint = 0;
            rcvbt.push_back(newint);
        }
        SET_BIT(rcvbt.at(index), bit % 32);
    }
    bool checkBit(int bit){
        int index = bit/32;
        if (rcvbt.size() <= index)
            return false;
        if (CHECK_BIT(rcvbt.at(index), bit % 32) == 0)
            return false;
        else
            return true;
    }
};

typedef unordered_map<SrcDst, ReassembledPacket*, SrcDstHasher> PMap;
typedef unordered_map<SrcDst6, ReassembledPacket*, SrcDst6Hasher> PMap6;

//#ifdef BWPLOT
class TimeBW {
public:
    struct timeval m_time;
    uint32_t m_bw;
    TimeBW(struct timeval t, uint32_t bw)
    {
        m_time = t;
        m_bw = bw;
    }
    ~TimeBW(){}
    bool operator<(const TimeBW& rhs) const {
        if (m_time.tv_sec == rhs.m_time.tv_sec)
            return m_time.tv_usec < rhs.m_time.tv_usec;
        return m_time.tv_sec < rhs.m_time.tv_sec;
    }
    bool operator==(const TimeBW& rhs) const {
        return m_bw < rhs.m_bw;
    }

 
};




class Spinlock
{
private:    //private copy-ctor and assignment operator ensure the lock never gets copied, which might cause issues.
    Spinlock operator=(const Spinlock & asdf);
    Spinlock(const Spinlock & asdf);
#ifdef __APPLE__
    OSSpinLock m_lock;
public:
    Spinlock()
    : m_lock(0)
    {}
    void lock() {
        OSSpinLockLock(&m_lock);
    }
    bool try_lock() {
        return OSSpinLockTry(&m_lock);
    }
    void unlock() {
        OSSpinLockUnlock(&m_lock);
    }
#else
    pthread_spinlock_t m_lock;
public:
    Spinlock() {
        pthread_spin_init(&m_lock, 0);
    }

    void lock() {
        pthread_spin_lock(&m_lock);
    }
    bool try_lock() {
        int ret = pthread_spin_trylock(&m_lock);
        return ret != 16;   //EBUSY == 16, lock is already taken
    }
    void unlock() {
        pthread_spin_unlock(&m_lock);
    }
    ~Spinlock() {
        pthread_spin_destroy(&m_lock);
    }
#endif
};





//#endif
#endif /* STRUCTS_H */
