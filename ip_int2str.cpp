#include <stdio.h>
#include <sys/stat.h>
#include <sstream>
#include <arpa/inet.h>
#include <netinet/if_ether.h> /* includes net/ethernet.h */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(int argc, char* argv[])
{  
    if (argc < 2) {
    	printf("Usage: %s <IP integer> \n", argv[0]);
    	exit(1);
    }
    int ip = atoi(argv[1]);
	char *an_addr;
	struct sockaddr_in ipip;
    ipip.sin_addr.s_addr = ip;
    an_addr = inet_ntoa(ipip.sin_addr);
    printf("%s\n", an_addr);
 }