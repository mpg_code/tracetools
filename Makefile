SRC=Parser.cpp FileManager.cpp DbManager.cpp Analyzer.cpp ProgressBar.cpp ListElem.cpp KMeansCluster.cpp LinReg.cpp
HEADERS=Parser.h FileManager.h DbManager.h Analyzer.h ipdefs.h structs.h ProgressBar.h ListElem.h KMeansCluster.h LinReg.h
CPP=clang++ 

UNAME := $(shell uname)

all: tt

ifeq ($(UNAME), Linux)
export PATH:=/usr/clang_3_4/bin:$(PATH)
CPP = $(shell PATH=$(PATH) which clang++)
ifeq ($(CPP), /usr/clang_3_4/bin/clang++)
	LIBCXXFLAGS = -stdlib=libc++
endif

tt: $(SRC) $(HEADERS) Makefile
	$(CPP) $(SRC) -std=c++11 $(LIBCXXFLAGS) -lpcap -pthread -lsqlite3 -O -o $@

debug: $(SRC) $(HEADERS) Makefile
	$(CPP) -g $(SRC) -std=c++11 $(LIBCXXFLAGS) -lpcap -pthread -lsqlite3 -O0 -o $@

ecdfp: ecdfp.cpp Makefile
	$(CPP) ecdfp.cpp -O3 -o $@

pdf_data: pdf_data.cpp Makefile
	$(CPP) pdf_data.cpp -lsqlite3 -O3 -o $@

ip2str: ip_int2str.cpp Makefile
	$(CPP) ip_int2str.cpp -O3 -o $@


endif

ifeq ($(UNAME), Darwin)

tt: $(SRC) $(HEADERS) Makefile
	$(CPP) -std=c++11 -stdlib=libc++ -lpcap -pthread -lsqlite3 -O -o $@ $(SRC)
debug: $(SRC) $(HEADERS) Makefile 
	$(CPP) -g -std=c++11 -stdlib=libc++ -lpcap -pthread -lsqlite3 -O -o $@ $(SRC)

ecdfp: ecdfp.cpp Makefile
	$(CPP) ecdfp.cpp -O3 -o $@

pdf_data: pdf_data.cpp Makefile
	$(CPP) pdf_data.cpp -lsqlite3 -O3 -o $@

ip2str: ip_int2str.cpp Makefile
	$(CPP) ip_int2str.cpp -O3 -o $@


endif


####################################################################
####################################################################
####################################################################

clean:
	rm -rf tt debug ecdfp ip2str a.out *~

