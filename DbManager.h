/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#ifndef _DB_MANAGER_H
#define _DB_MANAGER_H

#include "structs.h"
#include <sqlite3.h>
#include <vector>

class DbManager {
    public:
        DbManager(char* filename);
        ~DbManager();
        void writeDatabase(StreamDetails *sdetails, bool is_tcp, int ipv);
        void write(vector<StreamDetails*> *sdetailsVector, bool is_tcp, int ipv);
        pthread_mutex_t databaseTCP_mutex;
        pthread_mutex_t databaseTCPIP6_mutex;
        pthread_mutex_t databaseUDP_mutex;
        pthread_mutex_t databaseUDPIP6_mutex;
        int tcptable_entries, udptable_entries;

    protected:
        void connectDatabase();
        char* dbFilename;
        sqlite3* databaseTCP;
      //  sqlite3* databaseTCPIP6;
        sqlite3* databaseUDP;
       // sqlite3* databaseUDPIP6;
        pthread_mutexattr_t errorcheck;
};

#endif // _DB_MANAGER_H
