#ifndef _KMEANS_CLUSTER_H
#define _KMEANS_CLUSTER_H

#include <vector>
#include <list>
#include <math.h>
#include "ListElem.h"

class KMeansCluster {
    public:
        KMeansCluster(float mean);
        ~KMeansCluster();
        void updateMean();
        float getWithinClusterDistanceAvg();
        void mergeCluster(std::list<KMeansCluster>::iterator cluster);
        float distanceTo(std::list<KMeansCluster>::iterator cluster);
  //      bool containsIndex(uint32_t index);
    //    uint32_t getIatValue(uint32_t index);
        uint64_t getKeepAliveBreaks();
        void sort();

        float m_mean;
        std::vector<ListElem*> m_values;
};

#endif // _KMEANS_CLUSTER_H
