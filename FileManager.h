/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include "structs.h"

class FileManager
{
public:
    FileManager(bool isfile, char* name, char* dbFilename);
    ~FileManager();
    bool isEmpty();
    string nextFile(pcap_t **file);
    void write(string line, bool fin);
    int filesLeft();
    string outFilename;
    pthread_mutex_t file_mutex;
   
protected:
    void readDir(char* name);
    void readFile(char* name);
    list<string> filenames;
};

#endif /* FILEMANAGER_H */
