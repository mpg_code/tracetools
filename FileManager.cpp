/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#include <sstream>
#include "FileManager.h"

//-------------------------------------------------------------------------------------
bool compare_nocase (string first, string second)
{
    unsigned int i = 0;
    while ((i < first.length()) && (i < second.length()))
    {
        if (tolower(first[i]) < tolower(second[i])) return true;
        else if (tolower(first[i]) > tolower(second[i])) return false;
        ++i;
    }
    if (first.length() < second.length()) return true;
    else return false;
}
//-------------------------------------------------------------------------------------
FileManager::FileManager(bool isfile, char* name, char* dbFilename)
{
    if (isfile)
        readFile(name);
    else
        readDir(name);
    outFilename = dbFilename;
    outFilename = outFilename + "_train_length.txt";
    pthread_mutex_init(&file_mutex, NULL);
}
//-------------------------------------------------------------------------------------
FileManager::~FileManager()
{
}
//-------------------------------------------------------------------------------------
void FileManager::readDir(char* name)
{    
    DIR *cur_dir;
    struct dirent *entry;
    if( (cur_dir = opendir(name)) != NULL) {
        while((entry = readdir(cur_dir)) != NULL) {
            if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                string filename = name;
                filename = filename + "/" + entry->d_name;
                if (0 == filename.compare (filename.length() - 5, 5, ".pcap") || 
                    0 == filename.compare (filename.length() - 4, 4, ".cap") || 
                    0 == filename.compare (filename.length() - 4, 4, ".dmp"))  
                    filenames.push_back(filename);
            }
        }
        closedir(cur_dir);
    } else {
        cout << "'"<< name << "'" << ": no such directory." << endl;
        exit(1);
    }
    if (filenames.size() == 0){
        cout << "No dump files in this directory." << endl;
        exit(1);
    }
    filenames.sort(compare_nocase);
    for (list<string>::iterator it = filenames.begin(); it != filenames.end(); ++it) 
        cout << *it << endl;
}
//-------------------------------------------------------------------------------------
void FileManager::readFile(char* name)
{
    filenames.push_back(string(name));
} 
//-------------------------------------------------------------------------------------
bool FileManager::isEmpty()
{
    if (filenames.size() == 0)
        return true;
    else
        return false;
}
//-------------------------------------------------------------------------------------
int FileManager::filesLeft()
{
    return filenames.size();
}
//-------------------------------------------------------------------------------------
string FileManager::nextFile(pcap_t **file)
{  
    if (*file)
        pcap_close(*file);
    string filename = filenames.front();
    filenames.pop_front();
      
    char errbuf[PCAP_ERRBUF_SIZE];
    *file = pcap_open_offline(filename.c_str(), errbuf);
    
    if (!*file) {
        cerr << "Error reading file" << errbuf << endl;
        exit(1);
    }
    stringstream str_filename;
    int i = filename.length();
    while (i > 0 && filename[--i] != '/')
        str_filename << filename[i];

    string result = "";
    for (i = str_filename.str().length()-1; i >= 0; i--)
        result = result + str_filename.str()[i];

    return result;
}
//-------------------------------------------------------------------------------------
void FileManager::write(string line, bool fin)
{
    static FILE* tl = fopen(outFilename.c_str(), "w+");

    const char* ln = line.c_str();
    pthread_mutex_lock(&file_mutex);
    fwrite (ln, 1, line.length(), tl);
    if (fin)
        fclose(tl);
    pthread_mutex_unlock(&file_mutex);
}

