/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#include <stdio.h>
#include <sys/stat.h>
#include <sstream>
#include "Parser.h"
#include "ProgressBar.h"


#define WAIT_TIME_SECONDS 100
//-------------------------------------------------------------------------------------
void IPtoString (in_addr_t ip, stringstream *out)
{
    char *an_addr;
    struct sockaddr_in ipip;
    ipip.sin_addr.s_addr = ip;
    an_addr = inet_ntoa(ipip.sin_addr);
    *out << an_addr;
}
//-------------------------------------------------------------------------------------
Parser::Parser(bool isfile, char* name, char* dbFilename)
{
    pthread_mutexattr_init(&errorcheck);
    pthread_mutex_init(&map_mutex, &errorcheck);

    pthread_mutex_init(&ts_mutex, &errorcheck);
    pthread_mutex_init(&pmap_mutex, &errorcheck);
    pthread_mutex_init(&gsmap_mutex, &errorcheck);
    pthread_mutex_init(&pmap6tcp_mutex, &errorcheck);
    pthread_mutex_init(&pmap6udp_mutex, &errorcheck);
    pthread_mutex_init(&pmap_done_mutex, &errorcheck);
    pthread_mutex_init(&pmap_clean_mutex, &errorcheck);
    pthread_cond_init(&pmap_clean, NULL);
    int i;
    #ifdef NTEST
        pthread_mutex_init(&ntest_mutex, &errorcheck);
        for (i = 0; i < 100; i++)
            ntest_res[i] = 0;
    #endif 

    #ifdef MSS_PSH_TEST
        pthread_mutex_init(&msspsh_mutex, &errorcheck);
        int j;
        for (i = 0; i < 101; i++) {
            for (j = 0; j < 101; j++) {
                msspsh_conn[i][j] = 0;
                msspsh_data[i][j] = 0;
            }
        }
       /* for (i = 0; i < 10000; i++) {
            for (j = 0; j < 101; j++) {
                max_pl_conn[i][j] = 0;
                max_pl_data[i][j] = 0;
            }
        }*/
    #endif

    mTCPSMap = new SMap();
    mUDPSMap = new SMap();
    mTCPSMap6 = new SMap6();
    mUDPSMap6 = new SMap6();
    mRAPMap = new PMap();
    mRAPMap6TCP = new PMap6();
    mRAPMap6UDP = new PMap6();
    mGSMap = new GSMap();
    mFileManager = new FileManager(isfile, name, dbFilename);
    mDbManager = new DbManager(dbFilename);
   
    start_ts.tv_sec = 0;
    malformed_tcp = 0;
    malformed_udp = 0;
    src_port = 0, dst_port = 0;
    src_ip = 0, dst_ip = 0;
    protocol = 0;
    file = 0;
    data = 0;
    cleanPMap_started = false;
    packet_nr = 0;

    iphdr = 0;
    ip6hdr = 0;
    tcphdr = 0;
    udphdr = 0;

    curRAPacket = 0;
    curBufid = 0;
    curBufid6 = 0;

    pmap_done = false;
    new_pmap_entries = 0;
    ipv6_tot_headers = 0;
    
    parseTrace();

    cout << "TCP map size: " << mTCPSMap->size() << endl;
    cout << "UDP map size: " << mUDPSMap->size() << endl;
    cout << "TCP6 map size: " << mTCPSMap6->size() << endl;
    cout << "UDP6 map size: " << mUDPSMap6->size() << endl;
    cout << "TCP malformed packets: " << malformed_tcp << endl;
    cout << "UDP malformed packets: " << malformed_udp << endl;
    cout << "For more details about malformed packets (if any), see " << mFileManager->outFilename << endl;
    
  /*  if (cleanPMap_started) {
        pthread_mutex_lock(&pmap_done_mutex);
        pmap_done = true;
        pthread_mutex_unlock(&pmap_done_mutex);
        pthread_mutex_lock(&pmap_mutex);
        pthread_cond_signal(&pmap_clean);
        pthread_mutex_unlock(&pmap_mutex);
    }*/
}
//-------------------------------------------------------------------------------------
Parser::~Parser()
{
    delete mFileManager;
    delete mDbManager;
    delete mTCPSMap;
    delete mTCPSMap6;
    delete mUDPSMap;
    delete mUDPSMap6;
    delete mRAPMap6TCP;
    delete mRAPMap6UDP;
}
//-------------------------------------------------------------------------------------
void Parser::markECN(StreamStats *sstats)
{
    if (sstats->ce_packets < 65535) 
        sstats->ce_packets++;
    else {
        int i = 0;
        while (i < 8) {
            if (!CHECK_BIT(sstats->ce_16b_blocks,0)) {
                SET_BIT(sstats->ce_16b_blocks,0);
                break;
            } else 
                i++;
        }
        sstats->ce_packets = 0;
    }
}
//-------------------------------------------------------------------------------------
bool Parser::checkFlags(StreamStatsTCP *sstats)
{
    if (IS_RST(tcphdr)) {
        sstats->rst = 1;
        return false;
    } else if (IS_FIN(tcphdr)) { 
        sstats->fin = 1;
        if (pl_sz == 0) {
            timediff_sub(h.ts, sstats->init_time, sstats->prev_time);
        } 
    } else if (IS_SYN(tcphdr)) {
        sstats->first = 1;
        if (IS_ACK(tcphdr)) 
            sstats->synack = 1;
        else 
            sstats->syn = 1;
    } else if (IS_ACK(tcphdr) && (sstats->syn || sstats->synack) && sstats->first) {
        sstats->rtt_sample = 1;
        sstats->first = 0;
    } 
    if (IS_ECE(tcphdr) && !IS_SYN(tcphdr)) 
        ecn = 1;
    if (IS_PSH(tcphdr)) 
        psh = 1;
    if (!sstats->zero_win && WIN(tcphdr) == 0)
        sstats->zero_win = 1;
   
    return true;
}
//-------------------------------------------------------------------------------------
bool Parser::readIPHeader()
{
    if (start_ts.tv_sec == 0)
        start_ts = h.ts;

    packet_nr++;
   
    int nxt_hdr_offset;
    
    if (iphdr_offset >= h.caplen) // IP Header not captured
        return false;

    iphdr = (IPHDR*) (data + iphdr_offset);
    uint8_t tos;  
    int ipv = IPV(iphdr);
    if (ipv == 6) {
        return false;
        ip6hdr = (IP6HDR*) (data + iphdr_offset);
        iphdr = 0;
        src_ip6 = SADDR6(ip6hdr);
        dst_ip6 = DADDR6(ip6hdr);
        protocol = (uint8_t) PCOL6(ip6hdr);
        nxt_hdr_offset = iphdr_offset + sizeof(IP6HDR); // IPv6 header is 40 bytes
        tos = IP6_TC(ip6hdr);
        
        while (protocol == IPV6HOPBYHOP || protocol == IPV6ROUTE || protocol == IPV6FRAG) {
            if (nxt_hdr_offset >= h.caplen)
                return false;
            struct ip6_ext* ext_hdr = (struct ip6_ext*)(data + nxt_hdr_offset);
            nxt_hdr_offset += ext_hdr->ip6e_len;
            
            if (protocol == IPV6FRAG) 
                return reassembleIPPacket(nxt_hdr_offset);
            else if (protocol == IPV6HOPBYHOP)
                return false; // jumbogram, implement later
            protocol = (uint8_t)ext_hdr->ip6e_nxt;
        }
    } else {
        src_ip = SADDR(iphdr);
        dst_ip = DADDR(iphdr);
        protocol = (uint8_t) PCOL(iphdr);
        if (protocol != IPPROTO_TCP && protocol != IPPROTO_UDP)
            return false;
        if (MF(iphdr) || FO(iphdr) != 0)
            return reassembleIPPacket(0);
        int ihl = IHL(iphdr)*4;
        if (ihl < 20 || ihl > 60)
            return false;
        nxt_hdr_offset = iphdr_offset + ihl;
        tos = IP_TS(iphdr);
        
    }
    
    if (nxt_hdr_offset >= h.caplen) // only IP Header captured
        return false;

    if (protocol == IPPROTO_TCP) {
        tcphdr = (TCPHDR*) (data + nxt_hdr_offset);
        dst_port = DPORT(tcphdr);
        src_port = SPORT(tcphdr);
        is_tcp = 1;
        if (ipv == 6)
            ipv6_tot_headers = nxt_hdr_offset + DOFF(tcphdr)*4;
    } else if (protocol == IPPROTO_UDP) {
        udphdr = (UDPHDR*) (data + nxt_hdr_offset);
        src_port = UDP_SPORT(udphdr);
        dst_port = UDP_DPORT(udphdr);
        if (ipv == 6)
            ipv6_tot_headers = nxt_hdr_offset + 8;
    } else 
        return false;
  
    if (CHECK_BIT(tos,7) && CHECK_BIT(tos,6))
        ecn = 1;
    return true;
}
//-------------------------------------------------------------------------------------
bool Parser::reassembleIPPacket(int hdr_offset)
{
    uint16_t fo, ihl;
    ReassembledPacket *rapacket;

    if (protocol == IPV6FRAG) {
        return false;
        struct ip6_frag* frag_hdr = (struct ip6_frag*) (data + hdr_offset);
        protocol = (uint8_t)frag_hdr->ip6f_nxt;

        pthread_mutex_t *map6_mutex;
        PMap6* map6;
        if (protocol == IPPROTO_TCP) {
            map6_mutex = &pmap6tcp_mutex;
            map6 = mRAPMap6TCP;
        } else if (protocol == IPPROTO_UDP) {
            map6_mutex = &pmap6udp_mutex;
            map6 = mRAPMap6UDP;
        } else
            return false;
        
        uint32_t ident = ID6(frag_hdr);
        uint16_t ident_lo, ident_hi;
        ident_lo = (ident & 0xffff);
        ident_hi = (ident >> 16 & 0xffff);
       
        curBufid6 = new SrcDst6(src_ip6, ident_lo, dst_ip6, ident_hi);
        pthread_mutex_lock(map6_mutex);
        if (map6->find(*curBufid6) == map6->end()) {
            rapacket = new ReassembledPacket();
            timediff_sub(h.ts, start_ts, rapacket->diff_ts);
            map6->insert(make_pair(*curBufid6, rapacket));
            new_pmap_entries++;
        } else {
            rapacket = map6->at(*curBufid6); 
            timediff_sub(h.ts, start_ts, rapacket->diff_ts);
        }
        pthread_mutex_unlock(map6_mutex);
        fo = FO6(frag_hdr)*8;
        ihl = hdr_offset;
    } else {
        if (protocol != IPPROTO_TCP && protocol != IPPROTO_UDP) 
            return false;
       
        uint16_t ident = ID(iphdr);
        // use src_p to store identification and dst_p to store protocol
        curBufid = new SrcDst(src_ip, ident, dst_ip, (uint16_t)protocol);

        pthread_mutex_lock(&pmap_mutex);
        if (mRAPMap->find(*curBufid) == mRAPMap->end()) {
            rapacket = new ReassembledPacket();
            timediff_sub(h.ts, start_ts, rapacket->diff_ts);
            mRAPMap->insert(make_pair(*curBufid, rapacket));
            new_pmap_entries++;
        }
        else {
            rapacket = mRAPMap->at(*curBufid); 
            timediff_sub(h.ts, start_ts, rapacket->diff_ts);
        }
        fo = FO(iphdr)*8;
        ihl = IHL(iphdr)*4;
    }
    int size = h.caplen - ihl; 

    if (fo > MAX_FO)
        cout << "IP fragment offset is larger than the maximum of 65528: " << fo << endl;
    if (size > 0 && (fo < REASSEMBLE_BUFSZ)) {
        if (fo + size > REASSEMBLE_BUFSZ) 
            size = REASSEMBLE_BUFSZ - fo;
      //  if (!rapacket->b)
        //    rapacket->b = new Buffer();
        memcpy((void*)&(rapacket->b.buffer)[fo], (void*)(data + ihl), size);
    }

    int i;
    int from = FO(iphdr);
    int to = from + ((IP_LEN(iphdr)-ihl+7)/8);
    for (i = from; i <= to; i++) 
        rapacket->setBit(i);
            
    if (!MF(iphdr))  // last fragment
        rapacket->tdl = IP_LEN(iphdr) - ihl + fo;
    if (rapacket->tdl > 0) {
        // only check if the first fragment is received    
        if (rapacket->checkBit(0)) {
            if (curRAPacket) {
                delete curRAPacket;
                curRAPacket = 0;
                mRAPMap->erase(*curBufid);
                delete curBufid;
                curBufid = 0;
            }
            curRAPacket = rapacket;
            
            if (protocol == IPPROTO_TCP) {
                tcphdr = (TCPHDR*) rapacket->b.buffer;
                src_port = SPORT(tcphdr);
                dst_port = DPORT(tcphdr);
                is_tcp = 1;
            } else { // UDP
                udphdr = (UDPHDR*) rapacket->b.buffer;
                src_port = UDP_SPORT(udphdr);
                dst_port = UDP_DPORT(udphdr);
            } 
            pthread_mutex_unlock(&pmap_mutex);
            return true;
        } 
    } 
    pthread_mutex_unlock(&pmap_mutex);
    if (curBufid){
        delete curBufid;
        curBufid = 0;
    }
    if (curBufid6)
        delete curBufid6;
    
    
  /*  if (mRAPMap->size() >= PMAP_MAX_SIZE && new_pmap_entries >= PMAP_MAX_SIZE/2){
        new_pmap_entries = 0;
        pthread_cond_signal(&pmap_clean);
    }

    if (!cleanPMap_started) {
        pthread_t thread_id;
        parser_thread_param *tp = new parser_thread_param();
        tp->clean = &pmap_clean;
        tp->clean_mutex = &pmap_clean_mutex;
        tp->done_mutex = &pmap_done_mutex;
        tp->pmap_mutex = &pmap_mutex;
        tp->pmap = mRAPMap;
        tp->done = &pmap_done;
        tp->ts_mutex = &ts_mutex;
        tp->start_ts = start_ts;
        tp->cur_ts = &current_ts;
        pthread_attr_t attrs;
        pthread_attr_init(&attrs);
        pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_JOINABLE);   
        int res = pthread_create(&thread_id,&attrs,&Parser::cleanPMap,(void*)tp);
        cleanPMap_started = true;
    }*/

    return false;
}
//-------------------------------------------------------------------------------------
bool Parser::mapUDPStream(StreamStats **sstats)
{
    #ifdef MSS_PSH_TEST
        return 0;
    #endif
    pl_sz = payload_len_udp(udphdr);

    if (pl_sz == 0)
        return false;
    if (iphdr) {
        if (mUDPSMap->find(SrcDst(src_ip, src_port, dst_ip, dst_port)) == mUDPSMap->end()) {
            *sstats = new StreamStats();
            mUDPSMap->insert(make_pair(SrcDst(src_ip, src_port, dst_ip, dst_port), *sstats));
        } else
            *sstats = mUDPSMap->at(SrcDst(src_ip, src_port, dst_ip, dst_port));
    } else {
        if (mUDPSMap6->find(SrcDst6(src_ip6, src_port, dst_ip6, dst_port)) == mUDPSMap6->end()) {
            *sstats = new StreamStats();
            mUDPSMap6->insert(make_pair(SrcDst6(src_ip6, src_port, dst_ip6, dst_port), *sstats));
        } else
            *sstats = mUDPSMap6->at(SrcDst6(src_ip6, src_port, dst_ip6, dst_port));
    }

    if (iphdr && (pl_sz > MAX_PLSZ_UDP || (iphdr && pl_sz > (IP_LEN(iphdr) - IHL(iphdr)*4 - 8) && !curRAPacket))) {
        SET_BIT((*sstats)->malformed,0);
        return false;
    }
    return true;
}
//-------------------------------------------------------------------------------------
bool Parser::mapTCPStream(StreamStatsTCP **sstats)
{
    if (curRAPacket) 
        pl_sz = curRAPacket->tdl - DOFF(tcphdr)*4;
    else {
        if (iphdr)
            pl_sz = payload_len_tcp(iphdr, tcphdr); 
        else 
            pl_sz = IP6_LEN(ip6hdr) - ipv6_tot_headers;
    }
    if (iphdr) {
        if (mTCPSMap->count(SrcDst(src_ip, src_port, dst_ip, dst_port)) == 0) {
            *sstats = new StreamStatsTCP();
            mTCPSMap->insert(make_pair(SrcDst(src_ip, src_port, dst_ip, dst_port), (StreamStats*) *sstats));
        } else
            *sstats = (StreamStatsTCP*) mTCPSMap->at(SrcDst(src_ip, src_port, dst_ip, dst_port));
    } else { // ipv6
        if (mTCPSMap6->count(SrcDst6(src_ip6, src_port, dst_ip6, dst_port)) == 0) {
            *sstats = new StreamStatsTCP();
            mTCPSMap6->insert(make_pair(SrcDst6(src_ip6, src_port, dst_ip6, dst_port), (StreamStats*) *sstats));
        } else
            *sstats = (StreamStatsTCP*) mTCPSMap6->at(SrcDst6(src_ip6, src_port, dst_ip6, dst_port));
    }
    if (IS_SYN(tcphdr) && (*sstats)->init_seq != MAX_UINT32){
        if (CHECK_BIT((*sstats)->malformed,0) != 0)
            malformed_tcp++;
        else{
            vector<StreamDetails*> streamDetailsVectorTCP;
            streamDetailsVectorTCP.reserve(1);
            Analyzer::outputStreamStat(SrcDst(src_ip, src_port, dst_ip, dst_port), 
                (StreamStats*) (*sstats), &streamDetailsVectorTCP, true, mGSMap, &gsmap_mutex, mFileManager);
            mDbManager->write(&streamDetailsVectorTCP, true, 4);
        }
        unmapTCPStream(*sstats);
        *sstats = new StreamStatsTCP();
        mTCPSMap->insert(make_pair(SrcDst(src_ip, src_port, dst_ip, dst_port), (StreamStats*) *sstats));
    }   

    if ((iphdr && (pl_sz > MAX_PLSZ_TCP || ((DOFF(tcphdr) > (IP_LEN(iphdr) - IHL(iphdr)*4))))) ||
        (DOFF(tcphdr) < 5 || DOFF(tcphdr) > 15)) {
        SET_BIT((*sstats)->malformed,0);
        if (curRAPacket && curBufid) {
            delete curRAPacket;
            curRAPacket = 0;
            pthread_mutex_lock(&pmap_mutex);
            if (mRAPMap->count(*curBufid) != 0)
                mRAPMap->erase(*curBufid);
            pthread_mutex_unlock(&pmap_mutex);
            delete curBufid;
            curBufid = 0; 
        }
        return false;
    }
    return true;
}
//-------------------------------------------------------------------------------------
void Parser::unmapTCPStream(StreamStatsTCP *sstats)
{
    timediff_sub(h.ts, sstats->init_time, sstats->prev_time); 
      
    if (sstats == 0) {
        cerr << "trying to remove unexisting stream object" << endl;
        exit(1);
    }
    delete sstats;
    if (iphdr) {
        if (mTCPSMap->find(SrcDst(src_ip, src_port, dst_ip, dst_port)) == mTCPSMap->end()) {
            cerr << "tried to remove unexisting stream entry in mTCPSMap" << endl;
            exit(1);
        }
        mTCPSMap->erase(SrcDst(src_ip, src_port, dst_ip, dst_port));
    } else { // ipv6
        if (mTCPSMap6->find(SrcDst6(src_ip6, src_port, dst_ip6, dst_port)) == mTCPSMap6->end()) {
            cerr << "tried to remove unexisting stream entry in mTCPSMap" << endl;
            exit(1);
        }
        mTCPSMap6->erase(SrcDst6(src_ip6, src_port, dst_ip6, dst_port));
    }
}
//-------------------------------------------------------------------------------------
void Parser::StreamStat(StreamStats *sstats)
{
    sampleIAT(sstats);

    if (pl_sz == 0)
        return;
    if (!is_tcp || !IS_SYN(tcphdr))
        sstats->plList.push_back(pl_sz);
    if (is_tcp)
        ((StreamStatsTCP*)sstats)->prev_packet.pl_sz = pl_sz;    

    if (ecn) 
        markECN(sstats);
      
    if (sstats->headers_size == 0) {
        if (iphdr) {
            if (curRAPacket) {
                if (is_tcp)
                    sstats->headers_size = (uint8_t)(IHL(iphdr)*4 + DOFF(tcphdr)*4);
                else
                    sstats->headers_size = (uint8_t)(IHL(iphdr)*4 + 8);
            }
            else
                sstats->headers_size = (uint8_t)(IP_LEN(iphdr) - pl_sz);
        }
        else  // ipv6
            sstats->headers_size = ipv6_tot_headers;
    }
}
//-------------------------------------------------------------------------------------
bool Parser::checkSeq(StreamStatsTCP *sstats)
{    
    if (sstats->init_seq == MAX_UINT32) {
        sstats->init_seq = SEQ(tcphdr);
        if (!sstats->syn && !sstats->synack) {
            sstats->prev_packet.seq = 1;
            sstats->init_seq--;
        }
        return true;
    }
    
    uint32_t seq_abs = SEQ(tcphdr);
    uint32_t seq;
    if (seq_abs > sstats->init_seq)
        seq = seq_abs - sstats->init_seq;
    else 
        return false;
 
    uint32_t expected_seq = sstats->prev_packet.seq + sstats->prev_packet.pl_sz;
    if (sstats->prev_packet.seq == 0 && (sstats->prev_packet.pl_sz == 0 || pl_sz == 1))
        expected_seq++; // the first datapacket starts with seq 1 
    if (seq > sstats->prev_packet.seq || 
        (seq == sstats->prev_packet.seq && pl_sz > sstats->prev_packet.pl_sz) ) {
           
        if (seq > expected_seq) {
            uint64_t loss = seq - expected_seq;
            inc_loss = 1;
            if (loss > 1500000) 
                return false;
        }
        if (seq == sstats->prev_packet.seq && sstats->prev_packet.pl_sz > 0 && pl_sz > sstats->prev_packet.pl_sz) {
            // retransmitted packet that carries more payload
            sstats->plList.pop_back();
            if (sstats->iatVector.size() > 0) {
                iat_extra_delay += sstats->iatVector.back()->m_value;
                sstats->iatVector.pop_back();
            }
            if (sstats->prev_packet.pl_sz == 1) 
                sstats->keepalive = 1;
            else
                sstats->retr = 1;
            sstats->slow_start = 0;
            return true;
        } else if (seq == expected_seq - 1 && pl_sz <= 1) {
            sstats->keepalive = 1;
            sstats->slow_start = false;
            return false;
        }      
        if (sstats->slow_start) {
            int i = 0;
            // check if pl is at least >= min MSS
            if (!IS_SYN(tcphdr) && pl_sz >= 536 && seq == expected_seq) {
                while (i < CONSEQ_SEG) {
                    if (!CHECK_BIT(sstats->conseq_segments,i)) {
                        SET_BIT(sstats->conseq_segments, i);
                        break;
                    } else
                        i++;
                }
            } else {
                for (i = 0; i < CONSEQ_SEG; i++)
                    CLEAR_BIT(sstats->conseq_segments, i);
                sstats->slow_start = 0;                
            }
        }
        if (pl_sz > 0)
            sstats->prev_packet.seq = seq;
        else if (IS_ACK(tcphdr)) {
            uint32_t ack = ACK(tcphdr);
            if (ack > sstats->prev_packet.ack) {
                sstats->ackcount++;
                sstats->prev_packet.ack = ack;
            }
        }  
        return true;
    } else {
        if (seq == expected_seq - 1) 
            sstats->keepalive = 1;
        if (seq < sstats->prev_packet.seq)
            sstats->retr = 1;

        return false; // Don't count same packet twice
    }
}
//-------------------------------------------------------------------------------------
void estimate_rtt_slow_start(vector<ListElem*>& iatVector, int nr_samples, uint32_t synack_ack_sample)
{
    ListElem* samples[nr_samples];
    int i = 0;
    
    for (vector<ListElem*>::iterator it = iatVector.begin(); it != iatVector.end(); ++it) {
        samples[i++] = *it;
    }
    if (samples[0]->m_value > 5000 && samples[1]->m_value <= 5000 && 
        samples[2]->m_value > 5000 && samples[3]->m_value <= 5000 && samples[4]->m_value <= 5000) { // first burst consists of 2 segments
        // SS, init cwnd = 1 packets 
        if (samples[0]->m_value < synack_ack_sample) 
            samples[0]->m_rtt_ss = 1;
        if (samples[2]->m_value < synack_ack_sample)
            samples[2]->m_rtt_ss = 1;
    } else if (samples[0]->m_value <= 5000 && samples[1]->m_value > 5000 && 
            samples[2]->m_value <= 5000 && samples[3]->m_value <= 5000 && samples[4]->m_value <= 5000) {
         // SS, init cwnd = 2 packets
        if (samples[1]->m_value < synack_ack_sample)
            samples[1]->m_rtt_ss = 1;
    } else if (samples[0]->m_value <= 5000 && samples[1]->m_value <= 5000 && 
        samples[2]->m_value > 5000 && samples[3]->m_value <= 5000 && samples[4]->m_value <= 5000) {
        if (samples[2]->m_value < synack_ack_sample) 
            samples[2]->m_rtt_ss = 1;
    }
}

void Parser::sampleIAT(StreamStats *sstats) 
{ 
    struct timeval current_time = h.ts;
    if (sstats->init_time.tv_sec == 0 && sstats->init_time.tv_usec == 0) {
        sstats->init_time = current_time;
        timediff_sub(current_time, sstats->init_time, sstats->prev_time);
        if (sstats->prev_time.tv_sec > 11000) {
            cout << "setting init time, prev time: " << sstats->prev_time.tv_sec << ":" << sstats->prev_time.tv_usec << endl;
        }
        return;
    }
   
    struct timeval prev_time;
    bzero(&prev_time, sizeof(struct timeval));
    timediff_add(sstats->init_time, sstats->prev_time, prev_time);
    uint32_t ia_us = 0;
    timesub_timeval2us(current_time, prev_time, ia_us); // IAT in us
   
    if (pl_sz > 0) {
        timediff_sub(current_time, sstats->init_time, sstats->prev_time);
    }
    
    if (!is_tcp) {
        insertIATSample(ia_us, sstats);
        return;
    }
    StreamStatsTCP *ss = (StreamStatsTCP*) sstats;
    if (ss->rtt_sample) {
        ss->rtt_sample = 0;
        if (IS_ACK(tcphdr) && pl_sz == 0) {
            if (ss->syn) { //SYN - ACK RTT sample
                if (ia_us > 3000000 || ia_us < 5000) // may include RTO, skip
                    return;
                insertIATSample(ia_us, sstats, true, false, false); //rtt_sa
            } else if (ss->synack) {
                insertIATSample(ia_us, sstats, false, false, true); //sa_ack
                ss->slow_start = 1;
            } 
        } 
    } else if (ss->synack && ss->slow_start && conseqSegments(ss) == CONSEQ_SEG) { // SS sample
        ss->slow_start = 0;
        insertIATSample(ia_us, sstats, false, false, false);
        int nr_samples = ss->iatVector.size();
        uint32_t synack_ack_sample = 4294967295; // max uint32_t
        
        if (nr_samples == CONSEQ_SEG) {
            synack_ack_sample = ss->iatVector.front()->m_value;
            ss->iatVector.erase(ss->iatVector.begin());
            nr_samples--;
        } else if (nr_samples != CONSEQ_SEG-1) {
            cout << "wrong number of samples in the list: " << nr_samples << endl;
            cout << "seq:" << SEQ(tcphdr) - ss->init_seq << endl; 
            cout << src_ip << " " << dst_ip << " " << src_port << " " << dst_port << endl;
            exit(1);
        } 
        
        estimate_rtt_slow_start(ss->iatVector, nr_samples, synack_ack_sample);
    } else {
        if (pl_sz > 0 && ss->prev_packet.pl_sz > 0 && !ss->plList.empty())
            insertIATSample(ia_us, sstats, false, false, false); 
    }
} 
//-------------------------------------------------------------------------------------
void Parser::insertIATSample (uint32_t sample_us, StreamStats *sstats, bool rtt_sa, bool rtt_ss, bool sa_ack) 
{
    bool inc_keepalive = 0;
    bool inc_retr = 0;
    if (is_tcp) {
        inc_keepalive = ((StreamStatsTCP*)sstats)->keepalive;
        inc_retr = ((StreamStatsTCP*)sstats)->retr;
        ((StreamStatsTCP*)sstats)->retr = 0;
        ((StreamStatsTCP*)sstats)->keepalive = 0;
    }
    ListElem *newelem = new ListElem((sample_us+iat_extra_delay), rtt_sa, rtt_ss, sa_ack, inc_loss, inc_retr, inc_keepalive, psh);
    sstats->iatVector.push_back(newelem);
}
//-------------------------------------------------------------------------------------
int Parser::conseqSegments(StreamStatsTCP *sstats) {
    int i, cs = 0;
    for (i = 0; i < CONSEQ_SEG; i++){
        if (CHECK_BIT(sstats->conseq_segments, i))
            cs++;
    }
    return cs;
}
//-------------------------------------------------------------------------------------
void Parser::processData()
{
    vector<StreamDetails*> streamDetailsVectorTCP;
    streamDetailsVectorTCP.reserve(STREAM_DETAILS_VECTOR_SIZE);
     
    while (file) {
        data = pcap_next(file, &h);
        if (data == NULL) { 
            mDbManager->write(&streamDetailsVectorTCP, true, 4);
            return;
        }
        pl_sz = 0; 
        inc_loss = 0;
        psh = 0;
        iat_extra_delay = 0;
        is_tcp = 0;
        ecn = 0;
        protocol = 0;

        pthread_mutex_lock(&ts_mutex);
        current_ts = h.ts;
        pthread_mutex_unlock(&ts_mutex);
        if (!readIPHeader()) 
            continue;
        if (is_tcp) {
            StreamStatsTCP* sstats = 0;
            if (!mapTCPStream(&sstats))
                continue;
            if (checkFlags(sstats) && checkSeq(sstats))
                StreamStat((StreamStats*)sstats);
            else
                sstats->slow_start = 0;
            if (sstats->fin) {
                if (sstats->malformed)
                    malformed_tcp++;
                else
                    Analyzer::outputStreamStat(SrcDst(src_ip, src_port, dst_ip, dst_port), 
                        (StreamStats*) sstats, &streamDetailsVectorTCP, true, mGSMap, &gsmap_mutex, mFileManager);
                if (streamDetailsVectorTCP.size() >= STREAM_DETAILS_VECTOR_SIZE) 
                    mDbManager->write(&streamDetailsVectorTCP, true, 4);
                unmapTCPStream(sstats);
                continue;
            }
        } else {
            StreamStats* sstats = 0;
            if (mapUDPStream(&sstats))
                StreamStat((StreamStats*)sstats);
        }
        if (curRAPacket && curBufid){
            delete curRAPacket;
            curRAPacket = 0;
            pthread_mutex_lock(&pmap_mutex);
            if (mRAPMap->count(*curBufid) != 0)
                mRAPMap->erase(*curBufid);
            pthread_mutex_unlock(&pmap_mutex);
            delete curBufid;
            curBufid = 0;
        }
    }
}
//-------------------------------------------------------------------------------------
void Parser::parseTrace()
{
    int nrFiles = mFileManager->filesLeft();
    cout << "Parsing: " << nrFiles << " file(s)" << endl;
    ProgressBar::update(0);
    do {
        cur_filename = mFileManager->nextFile(&file);
        if (file) {
            int linktype = pcap_datalink(file);
            if (linktype == DLT_EN10MB) 
                iphdr_offset = ETHER_HDR_LEN;
            else if (linktype == DLT_RAW)
                iphdr_offset = 0;
            else if (linktype == DLT_C_HDLC)
                iphdr_offset = 4;
            else if (linktype == DLT_NULL)
                iphdr_offset = 4;
            else {
                cout << "Linktype: " << linktype << "not implemented yet. Feel free to do that :) or report" << endl;
                exit(1);
            }
            processData();
            ProgressBar::update((nrFiles - mFileManager->filesLeft())*100/nrFiles);
        }
    } while (!mFileManager->isEmpty());
    pcap_close(file);
}
//-------------------------------------------------------------------------------------
void* Parser::cleanPMap(void* param)
{
    parser_thread_param* tp = (parser_thread_param*) param;

    while(1) {

        pthread_mutex_lock(tp->clean_mutex);
        pthread_cond_wait(tp->clean, tp->clean_mutex);
        pthread_mutex_unlock(tp->clean_mutex);

        pthread_mutex_lock(tp->done_mutex);
        if (*(tp->done)) {
            pthread_mutex_unlock(tp->done_mutex);
            for (PMap::iterator itCur = tp->pmap->begin(); itCur != tp->pmap->end(); itCur++)
                delete itCur->second;
            delete tp->pmap;
            delete tp;
            return 0;
        }
        pthread_mutex_unlock(tp->done_mutex);

        pthread_mutex_lock(tp->ts_mutex);
        struct timeval cur_ts = *(tp->cur_ts);
        pthread_mutex_unlock(tp->ts_mutex);

        pthread_mutex_lock(tp->pmap_mutex);
        PMap::iterator itCur = tp->pmap->begin(); 
        PMap::iterator itEnd = tp->pmap->end(); 
        do {    
            ReassembledPacket* cur = itCur->second;
            struct timeval ts, diff;
            timediff_add(tp->start_ts, cur->diff_ts, ts);
            timersub(&cur_ts, &ts, &diff);
            if (diff.tv_sec > 100) {
                itCur = tp->pmap->erase(itCur);
                delete cur;
            } else
                itCur++;
        } while (itCur != itEnd);

        pthread_mutex_unlock(tp->pmap_mutex);
    }
    return 0;
}
//-------------------------------------------------------------------------------------
void usage(int argc, char* argv[])
{
    printf("Usage: %s  -f|-d  name -o databasefile -s statoption\n", argv[0]);
    printf("-f: single file, -d: directory containing trace files\n");
    printf("-o: an SQLite database file to output the results to. If it doesn't exist, it will be created, otherwise data is appended.\n");
    exit(1); 
}
//-------------------------------------------------------------------------------------
void start_threads(void* map, GSMap* gsmap, pthread_mutex_t *map_mutex, pthread_mutex_t *gsmap_mutex, DbManager *dbm, FileManager *fm, bool is_tcp, int* malformed_tcp, int* malformed_udp, int ipv)
{
    int num_threads = sysconf(_SC_NPROCESSORS_ONLN); // nr of cpu cores 
    #if defined (BWPLOT) || defined(DEBUG) || defined(LINREG)
        num_threads = 1;
        cout << "changing nr threads" << endl;
    #endif
    int i, res = 0;
    
    pthread_t thread_id[num_threads];
    uint64_t init_maplength; 
    thread_param tp;
    tp.num_threads = num_threads;
    tp.map_mutex = map_mutex;
    tp.gsmap = gsmap;
    tp.gsmap_mutex = gsmap_mutex;
    if (ipv == 4) {
        tp.map.smap = (SMap*) map;
        init_maplength = tp.map.smap->size();
    } else {
        tp.map.smap6 = (SMap6*) map;
        init_maplength = tp.map.smap6->size();
    }
    tp.cur_maplength = init_maplength;
    tp.dbm = dbm;
    tp.fm = fm;
    tp.is_tcp = is_tcp;
    tp.malformed_tcp = malformed_tcp;
    tp.malformed_udp = malformed_udp;
    
    pthread_attr_t attrs;
    pthread_attr_init(&attrs);
    pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_JOINABLE);             

    for (i = 0; i < num_threads; i++) {
        thread_id[i] = 0;
        if (ipv == 4)
            res = pthread_create(&thread_id[i],&attrs,&Analyzer::analyzeStreamMap,(void*)&tp);
        else
            res = pthread_create(&thread_id[i],&attrs,&Analyzer::analyzeStreamMap6,(void*)&tp);
        if (res != 0) {
            cerr << "Error while creating thread, exiting..." << endl;
            exit(1);
        } 
    }
    cout << "Started " << tp.num_threads << " threads." << endl;
    cout << "Analyzing..." << endl;
    
    bool threads_complete = false;
    int progress = 0;
    struct timespec ts;
    struct timeval ct;
    pthread_mutex_lock(&tp.update_progress_mutex);
    progress = (init_maplength - tp.cur_maplength)*100/init_maplength;
    cout << "init map length: " << init_maplength << endl;
    cout << "cur map length: " << tp.cur_maplength << endl;
    cout << "init progress: " << progress << endl;
    while (progress < 100) {
        res = gettimeofday(&ct, NULL);
        /* Convert from timeval to timespec */
        ts.tv_sec  = ct.tv_sec;
        ts.tv_nsec = ct.tv_usec * 1000;
        ts.tv_sec += WAIT_TIME_SECONDS;
        res = pthread_cond_timedwait(&tp.update_progress, &tp.update_progress_mutex, &ts);
        if (res != 0 && res != ETIMEDOUT)
            cout << "pthread_cond_timedwait returned: " << res << endl;
       
        if (res == ETIMEDOUT) {
            pthread_mutex_unlock(&tp.update_progress_mutex);
            cout << "Wait timed out!" << endl;
            cout << "Using the spinlock from now." << endl;
            int nr_threads_complete = 0;
            while (nr_threads_complete < num_threads) {
                sleep(15);
                nr_threads_complete = 0;
                tp.sl.lock();
                for (i = 0; i < num_threads; i++) {
                    if (tp.thread_complete[i] == 1) 
                        nr_threads_complete++;
                }
                tp.sl.unlock();
                cout << "Nr of threads complete: " << nr_threads_complete << endl;
                if (nr_threads_complete == num_threads) {
                    threads_complete = true;
                    ProgressBar::update(100);
                    break;
                } 
            }
        }
        int newprogress = (init_maplength - tp.cur_maplength)*100/init_maplength;       
        if (newprogress > progress){
            progress = newprogress;
            ProgressBar::update(progress);
        }
    }
    if (!threads_complete) {
        pthread_mutex_unlock(&tp.update_progress_mutex);
        cout << "Waiting for threads to complete..." << endl;
        for (i = 0; i < num_threads; i++) {  
            res = pthread_join(thread_id[i], NULL);
            if (res != ESRCH && res != 0)
                cerr << res << ": error joining thread " << i << endl;
            ProgressBar::update((i+1)*100/num_threads);
        }
    }
    if (ipv == 4)
        cout << "IPv4: ";
    else
        cout << "IPv6: ";
    if (is_tcp)
        cout << "DB entries written to TCP table: " << dbm->tcptable_entries << endl;
    else
        cout << "DB entries written to UDP table: " << dbm->udptable_entries << endl;
}
//-------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{  
    if (argc < 5) 
        usage(argc, argv);

    struct timeval start, end, total;
    gettimeofday(&start, NULL);
    
    Parser* parser;
    bool single_file;
    char* dbFilename = 0;

    if (strcmp(argv[1], "-f") == 0)
        single_file = true;
    else if (strcmp(argv[1], "-d") == 0)
        single_file = false;
    else
        usage(argc, argv);

    if (strcmp(argv[3], "-o") == 0)
        dbFilename = argv[4];
    else
        usage(argc, argv);
    #ifdef DEBUG
        mkdir("stream_class_txt",0777);
        mkdir("train_iat_cdf",0777);  
    #endif
   
    parser = new Parser(single_file, argv[2], argv[4]);
    
    if (parser->mTCPSMap->size() > 0)
        start_threads((void*)parser->mTCPSMap, parser->mGSMap, &(parser->map_mutex), &(parser->gsmap_mutex), parser->mDbManager, parser->mFileManager, 
            true, &parser->malformed_tcp, &parser->malformed_udp, 4);
    if (parser->mUDPSMap->size() > 0)    
        start_threads((void*)parser->mUDPSMap, parser->mGSMap, &(parser->map_mutex), &(parser->gsmap_mutex), parser->mDbManager, parser->mFileManager,
            false, &parser->malformed_tcp, &parser->malformed_udp, 4);
    if (parser->mTCPSMap6->size() > 0)    
        start_threads((void*)parser->mTCPSMap6, parser->mGSMap, &(parser->map_mutex), &(parser->gsmap_mutex), parser->mDbManager, parser->mFileManager, 
            true, &parser->malformed_tcp, &parser->malformed_udp, 6);
    if (parser->mUDPSMap6->size() > 0)    
        start_threads((void*)parser->mUDPSMap6, parser->mGSMap, &(parser->map_mutex), &(parser->gsmap_mutex), parser->mDbManager, parser->mFileManager, 
            false, &parser->malformed_tcp, &parser->malformed_udp, 6);

  //  cout << "Streams excluded due to malformed packets: " << parser->malformed << endl;
    parser->mFileManager->write("", true);
    gettimeofday(&end, NULL);
    timersub(&end, &start, &total);
    cout << "Total time used to deal with this dataset (h:m:s): " << total.tv_sec/3600 << 
            ":" << (total.tv_sec % 3600)/60 << ":" << total.tv_sec % 60 << endl;
    delete parser;
    return 0;
}


