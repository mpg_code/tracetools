#include <iostream>
#include <algorithm>
#include "KMeansCluster.h"

KMeansCluster::KMeansCluster(float mean)
{
	m_mean = mean;
}
//-------------------------------------------------------------------------------------
KMeansCluster::~KMeansCluster(){}
//-------------------------------------------------------------------------------------
void KMeansCluster::updateMean(){
	uint64_t nr_values = m_values.size();
	if (nr_values > 0) {
		float values_sum = 0;
		for (std::vector<ListElem*>::iterator it = m_values.begin(); it != m_values.end(); it++) 
			values_sum += float((*it)->m_value/1000);
		m_mean = values_sum / nr_values;
	}
}
//-------------------------------------------------------------------------------------
float KMeansCluster::getWithinClusterDistanceAvg() {
	float within_cluster_dist_avg = 0;
	if (m_values.size() > 1) {
		std::vector<ListElem*>::iterator it1;
		for (std::vector<ListElem*>::iterator it = m_values.begin(); it != m_values.end(); it++) {
			it1 = it;
			it1++;
			if (it1 == m_values.end())
				break;
			within_cluster_dist_avg += (*it1)->m_value - (*it)->m_value;
		}
		within_cluster_dist_avg /= (m_values.size() - 1);
	}
	return within_cluster_dist_avg;
}
//-------------------------------------------------------------------------------------
void KMeansCluster::mergeCluster(std::list<KMeansCluster>::iterator cluster)
{
	for (std::vector<ListElem*>::iterator it = cluster->m_values.begin(); it != cluster->m_values.end(); it++) 
		m_values.push_back(*it);
	updateMean();
}
//-------------------------------------------------------------------------------------
float KMeansCluster::distanceTo(std::list<KMeansCluster>::iterator cluster) 
{
	if ((*(m_values.crbegin()))->m_value < (*(cluster->m_values.begin()))->m_value)
		return ((*(cluster->m_values.begin()))->m_value - (*(m_values.crbegin()))->m_value)*1000;
	else 
		return ((*(m_values.begin()))->m_value - (*(cluster->m_values.crbegin()))->m_value)*1000;
}
//-------------------------------------------------------------------------------------
/*bool KMeansCluster::containsIndex(uint32_t index) 
{
	for (std::list<ListElem>::iterator it = m_values.begin(); it != m_values.end(); it++) 
		if (it->m_index == index)
			return true;
	return false;
}
//-------------------------------------------------------------------------------------
uint32_t KMeansCluster::getIatValue(uint32_t index) 
{
	for (std::list<ListElem>::iterator it = m_values.begin(); it != m_values.end(); it++) 
		if (it->m_index == index)
			return it->m_value;
	return 0;
}
//-------------------------------------------------------------------------------------*/
uint64_t KMeansCluster::getKeepAliveBreaks() 
{
	uint64_t keepalive_breaks = 0;
	for (std::vector<ListElem*>::iterator it = m_values.begin(); it != m_values.end(); it++) { 
		if ((*it)->m_inc_keepalive)
			keepalive_breaks++;
	}
	return keepalive_breaks;
}
//-------------------------------------------------------------------------------------*/
void KMeansCluster::sort()
{
	std::sort(m_values.begin(), m_values.end(), ListElem::compare); 
}