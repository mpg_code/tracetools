/*
-------------------------------------------------------------------------------------
    This source file is part of TraceTools.

    TraceTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.

    TraceTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TraceTools.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------------
*/
#ifndef PARSER_H
#define PARSER_H

#include "structs.h"
#include "FileManager.h"
#include "Analyzer.h"
#include "DbManager.h"

#define PMAP_MAX_SIZE 100000

struct parser_thread_param {
    pthread_cond_t *clean;
    pthread_mutex_t *clean_mutex, *done_mutex, *ts_mutex;
    pthread_mutex_t *pmap_mutex;
    PMap *pmap;
    bool *done;
    struct timeval start_ts;
    struct timeval* cur_ts;
};

class Parser
{
public:
    Parser(bool isfile, char* name, char* dbFilename);
    ~Parser();
    static void* cleanPMap(void* param);
    
    SMap* mTCPSMap;
    SMap* mUDPSMap;
    SMap6* mTCPSMap6;
    SMap6* mUDPSMap6;

    PMap* mRAPMap;
    PMap6* mRAPMap6TCP; 
    PMap6* mRAPMap6UDP;

    GSMap* mGSMap;

    FileManager* mFileManager;
    DbManager* mDbManager;
    pthread_mutex_t map_mutex;
    pthread_mutex_t gsmap_mutex;
   
    struct timeval current_ts;
    pthread_mutex_t ts_mutex;
    pthread_mutex_t pmap_mutex;
    pthread_mutex_t pmap6tcp_mutex;
    pthread_mutex_t pmap6udp_mutex; 
    pthread_mutex_t pmap_done_mutex;
    pthread_mutex_t pmap_clean_mutex;
    pthread_cond_t pmap_clean;
    bool pmap_done;
    struct timeval start_ts;
    int malformed_tcp;
    int malformed_udp;

protected:
    void markECN(StreamStats *sstats);
    bool checkFlags(StreamStatsTCP *sstats);
    bool readIPHeader();
    bool reassembleIPPacket(int hdr_offset);
    bool mapUDPStream(StreamStats **sstats);
    bool mapTCPStream(StreamStatsTCP **sstats);
    void unmapTCPStream(StreamStatsTCP *sstats);
    void StreamStat(StreamStats* sstats);
    bool checkSeq(StreamStatsTCP *sstats);
    void sampleIAT(StreamStats *sstats);
    void insertIATSample  (uint32_t sample_us, StreamStats *sstats, bool rtt_sa=false, bool rtt_ss=false, bool sa_ack=false);
    int conseqSegments(StreamStatsTCP *sstats);
    void processData();
    void parseTrace();
        
    uint64_t packet_nr;
    int is_tcp, ecn;
    uint16_t pl_sz;
    bool inc_loss;
    uint32_t iat_extra_delay;
    bool psh;
    
    int ipv6_tot_headers;
            
    IPHDR* iphdr;
    IP6HDR* ip6hdr;
    TCPHDR* tcphdr;
    UDPHDR* udphdr;

    uint16_t src_port, dst_port;
    in_addr_t src_ip, dst_ip;
    struct in6_addr src_ip6, dst_ip6;
    uint8_t protocol;
    bool cleanPMap_started;
    int new_pmap_entries;

    pcap_t *file;
 
    int iphdr_offset;
    const u_char *data;
    struct pcap_pkthdr h;

    pthread_mutexattr_t errorcheck;
    
    ReassembledPacket* curRAPacket;
    SrcDst *curBufid;
    SrcDst6 *curBufid6;

    string cur_filename;
};

#endif /* PARSER_H */
