#ifndef _LIST_ELEM_H
#define _LIST_ELEM_H

#include <stdio.h>
#include <stdint.h>
class ListElem {
public:
    ListElem(uint32_t value_usec, bool rtt_sa, bool rtt_ss, bool sa_ack, bool inc_loss, bool inc_retr, bool inc_keepalive, bool psh);
    ~ListElem();
    static bool compare(ListElem* lhs, ListElem* rhs) {
        return lhs->m_value < rhs->m_value;
    }
    uint32_t m_value;
   
    bool m_rtt_sa:1;
    bool m_rtt_ss:1;
    bool m_sa_ack:1;
    bool m_inc_loss:1;
    bool m_inc_retr:1;
    bool m_inc_keepalive:1;
    bool m_psh:1;
protected:
   
};

class GListElem {
public:
    GListElem(uint16_t pl_sz, uint32_t ts_ms)
    {
        m_pl_sz = pl_sz;
        m_ts_ms = ts_ms;
    }
    ~GListElem(){}
    uint16_t m_pl_sz;
    uint32_t m_ts_ms;
    
protected:
   
};
#endif // _LIST_ELEM_H