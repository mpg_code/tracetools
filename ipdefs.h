#ifndef IP_DEFS_H
#define IP_DEFS_H

#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <netinet/ip6.h>
#include <netinet/in.h>
#include <net/ethernet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

//defines for the packet type code in an ETHERNET header
#define ETHER_TYPE_IP (0x0800)
#define ETHER_TYPE_8021Q (0x8100)

#define	IP_DF 0x4000			/* dont fragment flag */
#define	IP_MF 0x2000			/* more fragments flag */
#define	IP_OFFMASK 0x1fff		/* mask for fragmenting bits */

#define MAX_FO 65528
#define MAX_PLSZ_UDP 65535
#define MAX_PLSZ_TCP 65495
#define MAX_IPV6_JUMBO_UDP 4294967295

// extension headers
#define IPV6HOPBYHOP 0x00
#define IPV4ENCAP 0x04
#define IPV6ENCAP 0x2C
#define IPV6ROUTE 0x2B
#define IPV6FRAG 0x2C
#define IPV6OPTS 0x3C

#if defined(__linux__)
#  define IPHDR struct iphdr
#  define IP6HDR struct ip6_hdr 
#  define ADDR6 __in6_u.__u6_addr32
#  define IP6FRAG struct ip6_frag
#  define IHL(iphdr)    iphdr->ihl
#  define IPV(iphdr)    iphdr->version
#  define IP_LEN(iphdr) ntohs(iphdr->tot_len)
#  define IP6_LEN(ip6hdr) (ntohs(ip6hdr->ip6_ctlun.ip6_un1.ip6_un1_plen) + 40)
#  define PCOL(iphdr)   iphdr->protocol
#  define PCOL6(ip6hdr)   ip6hdr->ip6_ctlun.ip6_un1.ip6_un1_nxt
#  define SADDR(iphdr)  iphdr->saddr
#  define SADDR6(ip6hdr) ip6hdr->ip6_src
#  define DADDR(iphdr)  iphdr->daddr
#  define DADDR6(ip6hdr) ip6hdr->ip6_dst
#  define ID(iphhdr)	ntohs(iphdr->id)
#  define ID6(ip6frag) ntohs(ip6frag->ip6f_ident)
#  define IP_OFF(iphdr) ntohs(iphdr->frag_off)
#  define IP6_OFF(ip6frag) ntohs(ip6frag->ip6f_offlg) 
#  define IP_TS(iphdr)  iphdr->tos
#  define IP6_TC(ip6hdr) (ip6hdr->ip6_ctlun.ip6_un1.ip6_un1_flow >> 20 & 0xff)

#  define UDPHDR struct udphdr
#  define UDP_SPORT(udphdr) ntohs(udphdr->source)
#  define UDP_DPORT(udphdr) ntohs(udphdr->dest)
#  define UDP_LEN(udphdr) ntohs(udphdr->len)

#  define TCPHDR struct tcphdr
#  define DOFF(tcphdr)  tcphdr->doff
#  define SEQ(tcphdr)   ntohl(tcphdr->seq)
#  define ACK(tcphdr)   ntohl(tcphdr->ack_seq)
#  define SPORT(tcphdr) ntohs(tcphdr->source)
#  define DPORT(tcphdr) ntohs(tcphdr->dest)

#  define IS_ACK(tcphdr) (tcphdr->ack)
#  define IS_SYN(tcphdr) (tcphdr->syn)
#  define IS_FIN(tcphdr) (tcphdr->fin)
#  define IS_RST(tcphdr) (tcphdr->rst)
#  define IS_PSH(tcphdr) (tcphdr->psh)
#  define IS_URG(tcphdr) (tcphdr->urg)
#  define IS_ECE(tcphdr) (tcphdr->res2 & 0x40)
#  define WIN(tcphdr) ntohs(tcphdr->window)


#else
#  define IPHDR struct ip
#  define IP6HDR struct ip6_hdr
#  define ADDR6 __u6_addr.__u6_addr32
#  define IP6FRAG struct ip6_frag
#  define IHL(iphdr)    iphdr->ip_hl
#  define IPV(iphdr)    iphdr->ip_v
#  define IP_LEN(iphdr) ntohs(iphdr->ip_len)
#  define IP6_LEN(ip6hdr) (ntohs(ip6hdr->ip6_ctlun.ip6_un1.ip6_un1_plen) + 40)
#  define PCOL(iphdr)   iphdr->ip_p
#  define PCOL6(ip6hdr)   ip6hdr->ip6_ctlun.ip6_un1.ip6_un1_nxt
#  define SADDR(iphdr)  iphdr->ip_src.s_addr
#  define SADDR6(ip6hdr) ip6hdr->ip6_src
#  define DADDR(iphdr)  iphdr->ip_dst.s_addr
#  define DADDR6(ip6hdr) ip6hdr->ip6_dst
#  define ID(iphhdr)	ntohs(iphdr->ip_id)
#  define ID6(ip6frag) ntohs(ip6frag->ip6f_ident)
#  define IP_OFF(iphdr) ntohs(iphdr->ip_off)
#  define IP6_OFF(ip6frag) ntohs(ip6frag->ip6f_offlg) 
#  define IP_TS(iphdr)  iphdr->ip_tos
#  define IP6_TC(ip6hdr) (ip6hdr->ip6_ctlun.ip6_un1.ip6_un1_flow >> 20 & 0xff)

#  define UDPHDR struct udphdr
#  define UDP_SPORT(udphdr) ntohs(udphdr->uh_sport)
#  define UDP_DPORT(udphdr) ntohs(udphdr->uh_dport)
#  define UDP_LEN(udphdr)   ntohs(udphdr->uh_ulen)

#  define TCPHDR struct tcphdr
#  define DOFF(tcphdr)  tcphdr->th_off
#  define SEQ(tcphdr)   ntohl(tcphdr->th_seq)
#  define ACK(tcphdr)   ntohl(tcphdr->th_ack)
#  define SPORT(tcphdr) ntohs(tcphdr->th_sport)
#  define DPORT(tcphdr) ntohs(tcphdr->th_dport)

#  define IS_ACK(tcphdr) (tcphdr->th_flags & TH_ACK)
#  define IS_SYN(tcphdr) (tcphdr->th_flags & TH_SYN)
#  define IS_FIN(tcphdr) (tcphdr->th_flags & TH_FIN)
#  define IS_RST(tcphdr) (tcphdr->th_flags & TH_RST)
#  define IS_PSH(tcphdr) (tcphdr->th_flags & TH_PUSH)
#  define IS_URG(tcphdr) (tcphdr->th_flags & TH_URG)
#  define IS_ECE(tcphdr) (tcphdr->th_flags & TH_ECE)
#  define WIN(tcphdr) ntohs(tcphdr->th_win)

#endif

inline uint16_t payload_len_tcp( const IPHDR* const iphdr, const TCPHDR* const tcphdr )
{
	return IP_LEN(iphdr) - IHL(iphdr)*4 - DOFF(tcphdr)*4;
}
inline uint16_t payload_len_udp(const UDPHDR* const udphdr )
{
	// udp header is always 8 bytes   
	return UDP_LEN(udphdr) - 8;
}

inline bool MF(const IPHDR* const iphdr)
{
	return (IP_OFF(iphdr)) & IP_MF;
}
inline uint16_t FO(const IPHDR* const iphdr)
{
	return (IP_OFF(iphdr)) & IP_OFFMASK;
}
inline bool MF6(const IP6FRAG* const ip6frag) 
{
	return (IP6_OFF(ip6frag)) & IP6F_MORE_FRAG;
}
inline bool FO6(const IP6FRAG* const ip6frag) 
{
	return (IP6_OFF(ip6frag)) & IP6F_OFF_MASK;
}

#endif /* IP_DEFS_H */

