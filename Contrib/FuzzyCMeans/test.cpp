#include "fuzzy_clustering.hpp"

using namespace Clustering;

int main(){

  static unsigned int number_points = 200;
  static unsigned int dimension_point = 1;
  static unsigned int number_clusters = 9;
  Matrix dataset(number_points, dimension_point);

  for (unsigned int i=0; i < 95; i++)
    for (unsigned int j=0; j < dimension_point; j++)
      dataset(i, j) = int(rand() % 5);
  for (unsigned int i=95; i < 190; i++)
    for (unsigned int j=0; j < dimension_point; j++)
      dataset(i, j) = int(10+rand() % 5);
  for (unsigned int i=190; i < 200; i++)
    for (unsigned int j=0; j < dimension_point; j++)
      dataset(i, j) = int(5000+rand() % 5000);

  std::cout << "Points (n_points X dim_point)"
	    <<std::endl<< " " <<dataset << std::endl;
 
  Fuzzy f(dataset, number_clusters);

  f.clustering();
}
