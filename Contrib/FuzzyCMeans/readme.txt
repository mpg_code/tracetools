Fuzzy C-Means belongs to a group of algorithms called fuzzy clustering algorithms.
Both its inputs and generated clusters are semantically similar to k-Means, but in
the spirit of fuzzy clustering, every element belongs to every cluster with a
certain probability.

This allows a certain amount of reasoning on the output of Fuzzy C-Means, which
is very hard to do with k-Means. In our example scenario of bursty traffic
detection, we may find the burst periods, but we may find several clusters for
the inter-burst-times, because there are so few of them. While k-Means requires
intuitive to clean up the mess, we can use the item probabilities to decide
whether it is reasonable to merge such centers with others.

The code is originally from here:
http://codingplayground.blogspot.no/2009/04/fuzzy-clustering.html
Please acknowledge Antonio Gulli when you use this code.
It required a some protection from divisions by zero, but otherwise it worked
out of the box.

Btw.: Gulli has a lot of other nice code, including a very recent C++
implementation of k-Means.

