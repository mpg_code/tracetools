#include "ListElem.h"
ListElem::ListElem(uint32_t value_usec, bool rtt_sa, bool rtt_ss, bool sa_ack, bool inc_loss, bool inc_retr, bool inc_keepalive, bool psh)
{
	m_value = value_usec;
	m_rtt_sa = rtt_sa;
	m_rtt_ss = rtt_ss;
	m_sa_ack = sa_ack;
	m_inc_loss = inc_loss;
	m_inc_retr = inc_retr;
	m_inc_keepalive = inc_keepalive;
	m_psh = psh;
}
ListElem::~ListElem(){}

/*GListElem::GListElem(uint32_t value_usec, bool rtt_sa, bool rtt_ss, bool sa_ack, bool inc_loss, bool inc_retr, bool inc_keepalive, bool psh)
{
	m_value = value_usec;
	m_rtt_sa = rtt_sa;
	m_rtt_ss = rtt_ss;
	m_sa_ack = sa_ack;
	m_inc_loss = inc_loss;
	m_inc_retr = inc_retr;
	m_inc_keepalive = inc_keepalive;
	m_psh = psh;
}
GListElem::~GListElem(){}*/
